﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

public class GameResultController : MonoBehaviour
{
    public static GameResultController instance;

    public TextMeshProUGUI goldText;
    public TextMeshProUGUI topScoreText;
    public TextMeshProUGUI rankText;
    public TextMeshProUGUI scoreText;

    public GameObject AdsNativePanel;
    public RawImage adIcon;
    public RawImage image;
    public TextMeshProUGUI adHeadline;
    public TextMeshProUGUI adAdvertiser;
    public TextMeshProUGUI ctaText;
    public AudioManager audioManager;

    public GameObject coinPrefab;
    public Transform coinStart;
    public Transform coinDes;
    public Image screenshot;

    public Transform lookImage;
    float rotateSpeed = 3;
    float radius = 8f;
    Vector2 center;
    float angle;
    public GameObject DownGroup;
    public GameObject NativeAdsArea;

    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
        AdsNativePanel.SetActive(false);
        AdsManager.instance.AdsNativePanel = AdsNativePanel;
        AdsManager.instance.adIcon = adIcon;
        AdsManager.instance.image = image;
        AdsManager.instance.adHeadline = adHeadline;
        AdsManager.instance.adAdvertiser = adAdvertiser;
        AdsManager.instance.ctaText = ctaText;
        AdsManager.instance.unifiedNativeAdLoaded = true;

        center = lookImage.transform.localPosition;
    }

    void Start()
    {
        //AdsManager.instance.RequestNativeAd();
        UpdateUi();
        UpdateRank();
        SetRankText();
        coinMove();
        ScreenshotLoad();
        AdsNativeUpdate();
    }   

    // Update is called once per frame
    void Update()
    {
        goldText.text = UserData.instance.gold.ToString();
        LookImageMove();
        AdsNativeUpdate();
    }

    void AdsNativeUpdate()
    {
        if (UserData.instance.isNoAds || !AdsManager.instance.isRewardAvailable())
        {
            NativeAdsArea.SetActive(false);
            DownGroup.transform.localPosition = new Vector3(0, 152, 0);
        }
        else
        {
            NativeAdsArea.SetActive(true);
            DownGroup.transform.localPosition = new Vector3(0, 0, 0);
        }
    }

    void LookImageMove()
    {
        angle -= rotateSpeed * Time.deltaTime;

        var offset = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle)) * radius;
        lookImage.transform.localPosition = center + offset;
    }

    void ScreenshotLoad()
    {
        var pathToImage = Application.persistentDataPath + "/DropTheNumberResult.png";
        //pathToImage = "DropTheNumberResult.png";
        Texture2D tex = null;
        byte[] fileByte;
        if(File.Exists(pathToImage) && UserData.instance.isShowCapture)
        {
            fileByte = File.ReadAllBytes(pathToImage);
            tex = new Texture2D(4, 4, TextureFormat.RGB24, false);
            tex.LoadImage(fileByte);
            screenshot.gameObject.SetActive(true);
            screenshot.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
            float x = Screen.width;
            float y = Screen.height;
            if (x / y < 0.5)
            {
                screenshot.transform.localScale = new Vector3(1, 1.1f, 1);
                screenshot.transform.localPosition = new Vector3(screenshot.transform.localPosition.x, -23.2f, screenshot.transform.localPosition.z);
                screenshot.fillAmount = 0.9f;               
            }
        }
    }

    public void coinMove()
    {
        if (UserData.instance.isResultCoinMove)
        {
            UserData.instance.isResultCoinMove = false;
            Sequence coinMove = DOTween.Sequence();
            audioManager.SoundCoin();

            coinMove.Insert(0.34f, DOTween.To(x => UserData.instance.gold = Mathf.FloorToInt(x), UserData.instance.gold, UserData.instance.gold + 10, 0.6f));
            coinMove.OnComplete(() => { 
                UserData.instance.Save(); 
            });
            for (int i = 0; i < 12; i++)
            {
                StartCoroutine(CoinAnimate(i, coinStart, coinDes));
            }
        }
    }

    IEnumerator CoinAnimate(int i, Transform moneySpawn, Transform moneyDes)
    {
        Sequence coinMove = DOTween.Sequence();
        float time = Random.Range(0, 0.35f);
        yield return new WaitForSeconds(time);
        GameObject coin = Instantiate(coinPrefab, moneyDes);
        coin.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
        coin.transform.position = moneySpawn.position + new Vector3(Random.Range(-0.2f, 0.2f), Random.Range(-0.2f, 0.2f), 0);
        yield return new WaitForSeconds(0.35f - time + i * 0.04f);

        coinMove.Insert(0, coin.transform.DOMove(moneyDes.position, 0.4f).OnComplete(() => {
            Destroy(coin.gameObject);
        }));
    }
    void UpdateUi()
    {
        goldText.text = UserData.instance.gold.ToString();
        topScoreText.text = UserData.instance.topScore.ToString();
        scoreText.text = GameController.instance.currentScore.ToString();
    }
    void SetRankText()
    {
        int number = UserData.instance.rank;
        if (number == 0)
        {
            rankText.text = "-";
        }
        else if (number % 10 == 1)
        {
            rankText.text = "#" + number.ToString() + "st";
        }
        else if (number % 10 == 2)
        {
            rankText.text = "#" + number.ToString() + "nd";
        }
        else if (number % 10 == 3)
        {
            rankText.text = "#" + number.ToString() + "rd";
        }
        else
        {
            rankText.text = "#" + number.ToString() + "th";
        }
        //rankText.text = # + UserData.instance.rank.ToString()
    }

    public void OnButtonBack()
    {
        UserData.instance.isShowCapture = false;
        AdsManager.instance.ShowAdsFullScreen();
        SceneManager.LoadScene("Menu");
    }
    public void OnButtonRestart()
    {
        UserData.instance.isShowCapture = false;
        AdsManager.instance.ShowAdsFullScreen();
        SceneManager.LoadScene("Game");
    }
    public void OnButtonGold()
    {
        //ads
        if (AdsManager.instance.isRewardAvailable())
        {
            AdsManager.instance.TypeReward = "RewardGold";
            AdsManager.instance.ShowAdsRewards();
        }
        else
        {
            Debug.Log("No ads available");
            SceneManager.LoadScene("PopUpAds", LoadSceneMode.Additive);
        }
    }

    public void RewardGold()
    {
        DOTween.To(x => UserData.instance.gold = Mathf.FloorToInt(x), UserData.instance.gold, UserData.instance.gold + 100, 0.25f).OnComplete(() => UserData.instance.Save());
        audioManager.SoundCoin();
    }

    public void OnButtonGoldPlus()
    {
        PlayerPrefs.SetString("PrevScene", "GameResult");
        SceneManager.LoadScene("Shop", LoadSceneMode.Additive);
    }

    public void OnButtonPig()
    {
        PlayerPrefs.SetString("PrevScene", "GameResult");
        SceneManager.LoadScene("Piggy", LoadSceneMode.Additive);
    }

    public void OnButtonRank()
    {
        PlayerPrefs.SetString("PrevScene", "GameResult");
        SceneManager.LoadScene("Rank", LoadSceneMode.Additive);
    }

    public void UpdateRank()
    {
        int number = UserData.instance.topScore;
        float rank = 0;
        if (number == 0)
        {
            rank = 0;
        }
        else if(number < 1000)
        {
            rank = 6000000 -  (number*1000) - Random.Range(0, 1000);
        }
        else if(number < 20000)
        {
            rank = 6000000 - (number * 250) - Random.Range(0, 250);
        }
        else if(number < 200000)
        {
            rank = 6000000 - (number * 29) - Random.Range(0, 100);
        }
        else if(number < 1000000)
        {
            rank = 200000 - (number - 200000) / 4.2f - Random.Range(0, 4);
        }
        else if(number < 2000000)
        {   
            rank = 10000 - (number - 1000000) / 111 - Random.Range(0, 111);
        }
        else if(number < 8000000)
        {
            rank = 1000 - (number - 2000000) / 6666;
        }
        if (rank < UserData.instance.rank || UserData.instance.rank == 0)
        {
            UserData.instance.rank = (int)rank;
        }
        UserData.instance.Save();
    }
}
