﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class HammerController : MonoBehaviour
{
    public static HammerController instance;

    private void Awake()
    {
        instance = this;
    }

    public TextMeshProUGUI goldText;
    public TextMeshProUGUI numHammer1text;
    public TextMeshProUGUI numHammer2text;
    public GameObject Hammer1stock;
    public GameObject Hammer1free;
    public GameObject Hammer2stock;
    public GameObject Hammer2free;
    public GameObject[] block;
    public GameObject Hammer1;
    public GameObject Hammer2;

    void Start()
    {
        Hammer1.SetActive(Hammer.instance.isHammer1);
        Hammer2.SetActive(Hammer.instance.isHammer2);
        UpdateUi();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateUi();
    }

    void UpdateUi()
    {
        goldText.text = UserData.instance.gold.ToString();
        for (int i = 0; i < 1; i++)
        {
            block[i].GetComponent<Image>().sprite = Hammer.instance.blockImage.sprite;
        }
        if (UserData.instance.numColorHammer > 0)
        {
            numHammer1text.text = UserData.instance.numColorHammer.ToString();
            Hammer1stock.SetActive(true);
            Hammer1free.SetActive(false);
        }
        else
        {
            Hammer1stock.SetActive(false);
            Hammer1free.SetActive(true);
        }

        if (UserData.instance.numHammer > 0)
        {
            numHammer2text.text = UserData.instance.numHammer.ToString();
            Hammer2stock.SetActive(true);
            Hammer2free.SetActive(false);
        }
        else
        {
            Hammer2stock.SetActive(false);
            Hammer2free.SetActive(true);
        }
    }

    public void OnButtonFree() //ads
    {
        if (AdsManager.instance.isRewardAvailable())
        {
            AdsManager.instance.TypeReward = "HammerFree";
            AdsManager.instance.ShowAdsRewards();           
        }
        else
        {
            Debug.Log("No ads available");
            SceneManager.LoadScene("PopUpAds", LoadSceneMode.Additive);
        }
    }

    public void HammerFree()
    {
        if (Hammer.instance.isHammer1)
        {
            UserData.instance.numColorHammer += 1;
        }
        if (Hammer.instance.isHammer2)
        {
            UserData.instance.numHammer += 1;
        }
        UserData.instance.Save();
    }

    public void OnButton100()
    {
        if (UserData.instance.gold < 100) return;
        UserData.instance.gold -= 100;
        if (Hammer.instance.isHammer1)
        {
            UserData.instance.numColorHammer += 1;
        }
        if (Hammer.instance.isHammer2)
        {
            UserData.instance.numHammer += 1;
        }
        UserData.instance.Save();
    }

    public void OnButton250()
    {
        if (UserData.instance.gold < 250) return;
        UserData.instance.gold -= 250;
        if (Hammer.instance.isHammer1)
        {
            UserData.instance.numColorHammer += 3;
        }
        if (Hammer.instance.isHammer2)
        {
            UserData.instance.numHammer += 3;
        }
        UserData.instance.Save();
    }

    public void OnButtonBack()
    {
        GameController.instance.ActivePause();
        Hammer.instance.isHammer1 = false;
        Hammer.instance.isHammer1 = false;
        SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName("Hammer"));
    }
    public void OnButtonHammer1()
    {
        Hammer.instance.isHammer1 = false;
        Hammer.instance.isHammer1 = false;
        GameController.instance.Pause();
        SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName("Hammer"));
        Hammer.instance.OnButtonHammerColor();
    }
    public void OnButtonHammer2()
    {
        Hammer.instance.isHammer1 = false;
        Hammer.instance.isHammer1 = false;
        GameController.instance.Pause();
        SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName("Hammer"));
        Hammer.instance.OnButtonHammer();
    }
}
