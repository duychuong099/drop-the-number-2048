﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Firebase;
using Firebase.Analytics;

public class LoadingController : MonoBehaviour
{
    public GameObject[] check;

    Firebase.FirebaseApp app;
    void Start()
    {
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                app = Firebase.FirebaseApp.DefaultInstance;

                // Set a flag here to indicate whether Firebase is ready to use by your app.
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
        UserData.instance.Load();
        StartCoroutine(Anim2());
    }


    IEnumerator Anim()
    {
        for (int i = 0; i < check.Length - 1; i++)
        {
            check[i].SetActive(true);
            yield return new WaitForSeconds(0.15f);
        }
        check[check.Length - 1].SetActive(true);
        SceneManager.LoadScene("Menu");
    }

    IEnumerator Anim2()
    {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("Menu");
    }
}
