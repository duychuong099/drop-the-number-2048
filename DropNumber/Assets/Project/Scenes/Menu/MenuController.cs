﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;
using System.Security.Cryptography;

public class MenuController : MonoBehaviour
{
    public static MenuController instance;

    private void Awake()
    {
        instance = this;
    }

    public Animator animMenu;
    public TextMeshProUGUI goldText;
    public TextMeshProUGUI topScoreText;
    public TextMeshProUGUI rankText;
    public Transform moneySpawn;
    public Transform moneyDes;
    public GameObject coinPrefab;
    public GameObject ExitPopup;

    public Button freeButton;
    public Button pigBankButton;

    public AudioManager audioManager;
    public MenuEffect menuEffect;

    Sequence coinMove;
    AsyncOperation scene1;

    private void Start()
    {              
        //animMenu.enabled = true;
        CheckHasPlayed();
        UserData.instance.Load();
        goldText.text = UserData.instance.gold.ToString();
        topScoreText.text = UserData.instance.topScore.ToString();
        SetRankText();
    }

    private void Update()
    {
        UpdateUI();
        //if (Application.platform == RuntimePlatform.Android)
        //{
            //if (Input.GetKeyDown(KeyCode.Escape))
            //{
                //Exit();
            //}
        //}
    }

    public void Exit()
    {
        ExitPopup.SetActive(true);
    }
    public void OnButtonYes()
    {
        Application.Quit();
    }

    public void OnButtonNo()
    {
        ExitPopup.SetActive(false);
    }

    public void OnButtonStart()
    {
        if(!UserData.instance.isFirstStartMenu)
        {
            AdsManager.instance.ShowAdsFullScreen();
        }
        if(UserData.instance.isFirstStartMenu)
        {
            UserData.instance.isFirstStartMenu = false;
        }
        menuEffect.DesTroyEffect();
        SceneManager.LoadScene("Game");
    }

    public void OnButtonGetGold() //ads buton
    {
        if (AdsManager.instance.isRewardAvailable())
        {
            AdsManager.instance.TypeReward = "MenuFree";
            AdsManager.instance.ShowAdsRewards();
        }
        else
        {
            Debug.Log("No ads available");
            SceneManager.LoadScene("PopUpAds", LoadSceneMode.Additive);
        }
    }

    public void getGold() //ads func
    {
        coinMove = DOTween.Sequence();
        
        //freeButton.gameObject.SetActive(false);
        audioManager.SoundCoin();
        for (int i = 0; i < 10; i++)
        {
            StartCoroutine(CoinAnimate(i));
        }
        coinMove.Insert(0.34f, DOTween.To(x => UserData.instance.gold = Mathf.FloorToInt(x), UserData.instance.gold, UserData.instance.gold + 100, 0.4f));
        coinMove.OnComplete(() => { UserData.instance.Save(); });
    }


    IEnumerator CoinAnimate(int i)
    {
        float time = Random.Range(0, 0.35f);
        yield return new WaitForSeconds(time);      
        GameObject coin = Instantiate(coinPrefab, moneyDes);

        coin.transform.position = moneySpawn.position + new Vector3(Random.Range(-0.2f, 0.2f), Random.Range(-0.2f, 0.2f), 0);
        yield return new WaitForSeconds(0.35f - time + i*0.04f);

        coinMove.Insert(0,coin.transform.DOMove(moneyDes.position, 0.4f).OnComplete(()=> {
            Destroy(coin.gameObject);            
        }));
    }

    public void UpdateUI()
    {
        goldText.text = UserData.instance.gold.ToString();
    }
    void SetRankText()
    {
        int number = UserData.instance.rank;
        if(number == 0)
        {
            rankText.text = "-";
        }
        else if(number % 10 == 1)
        {
            rankText.text = "#" + number.ToString() + "st";
        }
        else if(number % 10 == 2)
        {
            rankText.text = "#" + number.ToString() + "nd";
        }
        else if (number % 10 == 3)
        {
            rankText.text = "#" + number.ToString() + "rd";
        }
        else
        {
            rankText.text = "#" + number.ToString() + "th";
        }
        //rankText.text = # + UserData.instance.rank.ToString()
    }

    public void OnButtonGoldPlus()
    {
        PlayerPrefs.SetString("PrevScene", "Menu");
        SceneManager.LoadScene("Shop");
    }

    public void CheckHasPlayed()
    {
        if(!(PlayerPrefs.GetInt("HasPlayed", 0) == 1))
        {
            PlayerPrefs.SetInt("HasPlayed", 1);
            UserData.instance.gold = 0;
            UserData.instance.topScore = 0;
            UserData.instance.rank = 0;
            UserData.instance.gamePlayed = 0;
            UserData.instance.blockNum = new int[9];
            for (int i = 0; i < 9; i++)
            {
                UserData.instance.blockNum[i] = 0;
            }
            UserData.instance.numHammer = 1;
            UserData.instance.numColorHammer = 1;
            UserData.instance.numPiggy = 0;
            UserData.instance.isNoAds = false;
            UserData.instance.isPlaying = false;
            UserData.instance.Save();
        }
    }

    public void Clear()
    {
        Debug.Log("Clear");
        PlayerPrefs.DeleteAll();
    }
    public void OnButtonPiggy()
    {
        PlayerPrefs.SetString("PrevScene", "Menu");
        SceneManager.LoadScene("Piggy", LoadSceneMode.Additive);
    }

    public void OnButtonRank()
    {
        PlayerPrefs.SetString("PrevScene", "Menu");
        SceneManager.LoadScene("Rank", LoadSceneMode.Additive);
    }
}
