﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class PauseController : MonoBehaviour
{
    public Toggle volume;
    public Toggle vibrate;
    public GameObject AdsNativePanel;
    public RawImage adIcon;
    public RawImage image;
    public TextMeshProUGUI adHeadline;
    public TextMeshProUGUI adAdvertiser;
    public TextMeshProUGUI ctaText;
    public GameObject DownGroup;
    public GameObject NativeAdsArea;

    private void Awake()
    {
        AdsNativePanel.SetActive(false);
        AdsManager.instance.AdsNativePanel = AdsNativePanel;
        AdsManager.instance.adIcon = adIcon;
        AdsManager.instance.image = image;
        AdsManager.instance.adHeadline = adHeadline;
        AdsManager.instance.adAdvertiser = adAdvertiser;
        AdsManager.instance.ctaText = ctaText;
        AdsManager.instance.unifiedNativeAdLoaded = true;

        GameController.instance.DeActiveCamera();
    }

    void Start()
    {
        //AdsManager.instance.RequestNativeAd();
        if ((PlayerPrefs.GetInt("isSound", 1) == 1))
        {
            volume.isOn = false;
        }
        else
        {
            volume.isOn = true;
        }
        if ((PlayerPrefs.GetInt("isVibrate", 1) == 1))
        {
            vibrate.isOn = false;
        }
        else
        {
            vibrate.isOn = true;
        }
        AdsNativeUpdate();
    }

    private void Update()
    {
        AdsNativeUpdate();
    }

    void AdsNativeUpdate()
    {
        if (UserData.instance.isNoAds || !AdsManager.instance.isRewardAvailable())
        {
            NativeAdsArea.SetActive(false);
            DownGroup.transform.localPosition = new Vector3(0, 108, 0);
        }
        else
        {
            NativeAdsArea.SetActive(true);
            DownGroup.transform.localPosition = new Vector3(0, 0, 0);
        }
    }

    public void OnButtonContinue()
    {
        AdsManager.instance.ShowAdsFullScreen();
        GameController.instance.ActivePause();
        GameController.instance.ActiveCamera();
        SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName("Pause"));
    }
    public void OnButtonRestart()
    {
        AdsManager.instance.ShowAdsFullScreen();
        UserData.instance.gamePlayed += 1;
        UserData.instance.Save();
        SceneManager.LoadScene("Game");
    }
    public void OnButtonHome()
    {
        AdsManager.instance.ShowAdsFullScreen();
        SceneManager.LoadScene("Menu");
    }
    public void OnVolumeChange()
    {
        if(volume.isOn)
        {
            PlayerPrefs.SetInt("isSound", 0);
            UserData.instance.isSound = (PlayerPrefs.GetInt("isSound", 1) == 1);
        }
        else
        {
            PlayerPrefs.SetInt("isSound", 1);
            UserData.instance.isSound = (PlayerPrefs.GetInt("isSound", 1) == 1);
        }
    }

    public void OnVibrateChange()
    {
        if (vibrate.isOn)
        {
            PlayerPrefs.SetInt("isVibrate", 0);
            UserData.instance.isSound = (PlayerPrefs.GetInt("isSound", 1) == 1);
        }
        else
        {
            PlayerPrefs.SetInt("isVibrate", 1);
            UserData.instance.isSound = (PlayerPrefs.GetInt("isSound", 1) == 1);
            Handheld.Vibrate();
            //vibrate
        }
    }
}
