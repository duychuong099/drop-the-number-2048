﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PiggyController : MonoBehaviour
{
    public TextMeshProUGUI goldText;
    public Slider slider;

    public void Start()
    {
        goldText.text = UserData.instance.gold.ToString();
    }

    private void Update()
    {
        goldText.text = UserData.instance.gold.ToString();
        if (UserData.instance.numPiggy != 0 && UserData.instance.numPiggy < 135)
        {
            slider.value = 135;
        }
        else slider.value = UserData.instance.numPiggy;
    }

    public void OnButtonBack()
    {
        if (PlayerPrefs.GetString("PrevScene", "Menu") == "Game")
        {
            GameController.instance.ActivePause();
            SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName("Piggy"));
        }
       else  SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName("Piggy"));
    }


}
