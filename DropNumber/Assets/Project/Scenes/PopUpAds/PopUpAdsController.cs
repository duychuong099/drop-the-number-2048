﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PopUpAdsController : MonoBehaviour
{
    public void OnButtonOk()
    {
        if(PlayerPrefs.GetInt("isPopUpAdsPause", 0)==1)
        {
            PlayerPrefs.SetInt("isPopUpAdsPause", 0);
            GameController.instance.ActivePause();
        }
        SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName("PopUpAds"));
    }
}
