﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class RankController : MonoBehaviour
{
    public TextMeshProUGUI goldText;
    public TextMeshProUGUI rankText;
    public TextMeshProUGUI topScoreText;
    public TextMeshProUGUI nameText;
    public AudioManager audioManager;

    void Start()
    {
        UserData.instance.Load();
        SetRankText();
        goldText.text = UserData.instance.gold.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        goldText.text = UserData.instance.gold.ToString();
    }

    public void OnButtonBack()
    {
        AdsManager.instance.ShowAdsFullScreen();
        if (PlayerPrefs.GetString("PrevScene", "Menu") == "Game")
        {
            GameController.instance.ActivePause();
            SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName("Rank"));
        }
        else SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName("Rank"));
    }
    void SetRankText()
    {
        int number = UserData.instance.rank;

        rankText.text = number.ToString();
        topScoreText.text = UserData.instance.topScore.ToString();
        //nameText.text = PlayGamesPlatform.Instance.RealTime.GetSelf().Displayname;
    }
}
