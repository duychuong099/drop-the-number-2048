﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.SceneManagement;

public class RecordController : MonoBehaviour
{
    public TextMeshProUGUI rankText;
    public TextMeshProUGUI topScoreText;
    public TextMeshProUGUI gamePlayedText;
    public TextMeshProUGUI[] blockText;

    void Start()
    {
        UserData.instance.Load();
        topScoreText.text = UserData.instance.topScore.ToString();
        gamePlayedText.text = UserData.instance.gamePlayed.ToString();
        SetRankText();
        SetBlockNum();
    }

    void SetRankText()
    {
        int number = UserData.instance.rank;
        if (number % 10 == 1)
        {
            rankText.text = "#" + number.ToString() + "st";
        }
        else if (number % 10 == 2)
        {
            rankText.text = "#" + number.ToString() + "nd";
        }
        else if (number % 10 == 3)
        {
            rankText.text = "#" + number.ToString() + "rd";
        }
        else
        {
            rankText.text = "#" + number.ToString() + "th";
        }
        //rankText.text = # + UserData.instance.rank.ToString()
    }

    void SetBlockNum()
    {
        for (int i = 0; i < 9; i++)
        {
            blockText[i].text = UserData.instance.blockNum[i].ToString();
        }
    }
    public void OnButtonBack()
    {
        GameController.instance.ActivePause();
        SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName("Record"));
    }

    public void OnButtonShared()
    {
    }
}
