﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UnityEngine.UI;   

public class ShopController : MonoBehaviour
{
    public static ShopController instance;
    private void Awake()
    {
        instance = this;
    }

    public TextMeshProUGUI goldText;

    public GameObject coinPrefab;
    public Transform moneySpawn;
    public Transform moneyDes;

    public Image[] mask;

    public AudioManager audioManager;

    Sequence coinMove;
    void Start()
    {
        UserData.instance.Load();
        goldText.text = UserData.instance.gold.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        goldText.text = UserData.instance.gold.ToString();
    }

    public void OnButtonGetGold() //ads
    {
        if (AdsManager.instance.isRewardAvailable())
        {
            AdsManager.instance.TypeReward = "ShopGetGold";
            AdsManager.instance.ShowAdsRewards();          
        }
        else
        {
            Debug.Log("No ads available");
            SceneManager.LoadScene("PopUpAds", LoadSceneMode.Additive);
        }
    }

    public void ShopGetGold()
    {
        coinMove = DOTween.Sequence();
        //freeButton.gameObject.SetActive(false);
        for (int i = 0; i < 10; i++)
        {
            StartCoroutine(CoinAnimate(i));
        }
        coinMove.Insert(0.34f, DOTween.To(x => UserData.instance.gold = Mathf.FloorToInt(x), UserData.instance.gold, UserData.instance.gold + 100, 0.4f));
        coinMove.OnComplete(() => { UserData.instance.Save(); });
    }

    IEnumerator CoinAnimate(int i)
    {
        float time = Random.Range(0, 0.35f);
        yield return new WaitForSeconds(time);
        GameObject coin = Instantiate(coinPrefab, moneySpawn);
        coin.transform.position = moneySpawn.position + new Vector3(Random.Range(-20, 20), Random.Range(-20, 20), 0);
        yield return new WaitForSeconds(0.35f - time + i * 0.04f);

        coinMove.Insert(0, coin.transform.DOMove(moneyDes.position, 0.4f).OnComplete(() => {
            Destroy(coin.gameObject);
        }));
    }

    public void OnButtonBack()
    {
        AdsManager.instance.ShowAdsFullScreen();
        FadeOut();
    }

    void FadeOut()
    {
        if(PlayerPrefs.GetString("PrevScene", "Menu") == "Game")
        {
            GameController.instance.ActivePause();
            SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName("Shop"));
        }
        else if(PlayerPrefs.GetString("PrevScene", "Menu") == "Revine")
        {
            SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName("Shop"));
        }
        else SceneManager.LoadScene(PlayerPrefs.GetString("PrevScene", "Menu"));
    }
}
