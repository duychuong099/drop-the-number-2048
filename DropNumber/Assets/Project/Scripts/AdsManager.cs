﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using GoogleMobileAds;
using UnityEngine.UI;
using TMPro;
using System;
using System.Net;
using System.IO;

public class AdsManager : MonoBehaviour
{
    GameObject icon;
    public static AdsManager instance;
    public GameObject AdsNativePanel;
    public RawImage adIcon;
    public RawImage image;
    public TextMeshProUGUI adHeadline;
    public TextMeshProUGUI adAdvertiser;
    public TextMeshProUGUI ctaText;
    public string TypeReward = "";
    public bool isShowFail = false;
    public bool isConection { get; set; } = false;
    string x;
    float timer = 0;
    float deltaTime = 1;

    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this);
        AdsNativePanel.SetActive(false);
    }

    private BannerView bannerView;
    private InterstitialAd interstitial;
    private RewardBasedVideoAd rewardBasedVideo;

    public bool unifiedNativeAdLoaded;
    public UnifiedNativeAd nativeAd { get; set; }

    void Start()
    {
        MobileAds.Initialize(initStatus => { });
        if (!UserData.instance.isNoAds)
        {
            this.RequestBanner();
            RequestNativeAd();
        }
        RequestInterstitial();
        this.rewardBasedVideo = RewardBasedVideoAd.Instance;
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        RequestRewardBasedVideo();
        FirstCheckConnect();
    }
    
    private void Update()
    {
        if (this.unifiedNativeAdLoaded && !UserData.instance.isNoAds)
        {
            this.unifiedNativeAdLoaded = false;
            // Get Texture2D for icon asset of native ad.
            Texture2D iconTexture = this.nativeAd.GetIconTexture();
            Texture2D iconAdChoices = this.nativeAd.GetAdChoicesLogoTexture();
            Texture2D imageTexture = this.nativeAd.GetImageTextures()[0];
            // Get string for headline asset of native ad.
            string headline = this.nativeAd.GetHeadlineText();
            string cta = this.nativeAd.GetCallToActionText();
            string advertiser = this.nativeAd.GetAdvertiserText();

            adIcon.texture = iconTexture;
            adHeadline.text = headline;
            adAdvertiser.text = advertiser;
            ctaText.text = cta;
            image.texture = imageTexture;

            nativeAd.RegisterIconImageGameObject(adIcon.gameObject);
            nativeAd.RegisterHeadlineTextGameObject(adHeadline.gameObject);
            nativeAd.RegisterAdvertiserTextGameObject(adAdvertiser.gameObject);
            nativeAd.RegisterCallToActionGameObject(ctaText.gameObject);

            AdsNativePanel.SetActive(true);
        }
        timer += Time.deltaTime;
        if(timer > deltaTime)
        {
            CheckConnect();
            timer = 0;
        }
    }

    //void OnGUI()
    //{
        //int w = Screen.width, h = Screen.height;

        //GUIStyle style = new GUIStyle();

        //Rect rect = new Rect(0, 0, w, h * 2 / 100);
        //style.alignment = TextAnchor.UpperLeft;
        //style.fontSize = h * 2 / 100;
        //style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
        //string text = x;
        //GUI.Label(rect, text, style);
    //}

    private void RequestBanner()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-3940256099942544/6300978111"; //banner
#elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-3940256099942544/2934735716";
#else
            string adUnitId = "unexpected_platform";
#endif

        // Create a 320x50 banner at the top of the screen.
        this.bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        // Load the banner with the request.
        this.bannerView.LoadAd(request);

        bannerView.Show();
    }
    public void DestroyBanner()
    {
        if(bannerView!=null)
        {
            bannerView.Hide();
            bannerView.Destroy();
        }
    }

    private void RequestInterstitial()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-3940256099942544/1033173712"; //inter
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/4411468910";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Initialize an InterstitialAd.
        this.interstitial = new InterstitialAd(adUnitId);

        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        this.interstitial.LoadAd(request);
    }

    public void ShowAdsFullScreen()
    {
        //show ads fullscreen
        if (this.interstitial.IsLoaded() && !UserData.instance.isNoAds)
        {
            UserData.instance.isNextPause = false;
            this.interstitial.Show();
            RequestInterstitial();
        }
    }

    private void RequestRewardBasedVideo()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-3940256099942544/5224354917"; //reward
#elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-3940256099942544/1712485313";
#else
            string adUnitId = "unexpected_platform";
#endif

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded video ad with the request.
        this.rewardBasedVideo.LoadAd(request, adUnitId);
    }

    public void ShowAdsRewards()
    {
        if (rewardBasedVideo.IsLoaded())
        {
            //Show ads reward
            UserData.instance.isNextPause = false;
            rewardBasedVideo.Show();
            RequestRewardBasedVideo();
        }
    }

    public bool isRewardAvailable()
    {
        if (isConection)
        {
            return rewardBasedVideo.IsLoaded();
        }
        else return false;
    }

    public void RequestNativeAd()
    {
        AdLoader adLoader = new AdLoader.Builder("ca-app-pub-3940256099942544/1044960115") //native
            .ForUnifiedNativeAd()
            .Build();
        adLoader.OnUnifiedNativeAdLoaded += this.HandleUnifiedNativeAdLoaded;
        adLoader.OnNativeAdClosed += this.HandleOnNativeAdClosed;
        adLoader.LoadAd(new AdRequest.Builder().Build());
    }

    private void HandleUnifiedNativeAdLoaded(object sender, UnifiedNativeAdEventArgs args)
    {
        MonoBehaviour.print("Unified Native ad loaded.");
        this.nativeAd = args.nativeAd;
        this.unifiedNativeAdLoaded = true;
    }
    private void HandleOnNativeAdClosed(object sender, EventArgs args)
    {
        AdsNativePanel.SetActive(false);
        RequestNativeAd();
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        MonoBehaviour.print(
            "HandleRewardBasedVideoRewarded event received for "
                        + amount.ToString() + " " + type);

        switch(TypeReward)
        {
            case "MenuFree": MenuController.instance.getGold();break;
            case "VideoPreview": Boost.instance.PopupPreview.GetComponent<Boost>().VideoPreview();break;
            case "Video500": Boost.instance.Popup500.GetComponent<Boost>().Video500(); break;
            case "HammerFree": HammerController.instance.HammerFree(); break;
            case "SmallFreePreviewButton": UiController.instance.SmallFreePreviewButton();break;
            case "ShopGetGold": ShopController.instance.ShopGetGold(); break;
            case "Claim30": UiController.instance.Claim30(); break;
            case "Claim150": GameController.instance.Claim150(); break;
            case "RewardGold": GameResultController.instance.RewardGold(); break;
            case "Revine": RevineController.instance.Revine(); break;
            case "Piggy": UiController.instance.ActivePiggy();break;
        }
    }
    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardBasedVideoFailedToLoad event received with message: "
                             + args.Message);
        isShowFail = true;
        x = args.Message;
    }

    public string GetHtmlFromUri(string resource)
    {
        string html = string.Empty;
        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(resource);
        try
        {
            using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
            {
                bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
                if (isSuccess)
                {
                    using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
                    {
                        //We are limiting the array to 80 so we don't have
                        //to parse the entire html document feel free to 
                        //adjust (probably stay under 300)
                        char[] cs = new char[80];
                        reader.Read(cs, 0, cs.Length);
                        foreach (char ch in cs)
                        {
                            html += ch;
                        }
                    }
                }
            }
        }
        catch
        {
            return "";
        }
        return html;
    }

    public void FirstCheckConnect()
    {
        string HtmlText = GetHtmlFromUri("http://google.com");
        if (HtmlText == "")
        {
            isConection = false;
        }
        else if (!HtmlText.Contains("schema.org/WebPage"))
        {
            //Redirecting since the beginning of googles html contains that 
            //phrase and it was not found
        }
        else
        {
            isConection = true;
            //success
        }
    }
    public void CheckConection()
    {
        string HtmlText = GetHtmlFromUri("http://google.com");
        if (HtmlText == "")
        {
            if(isConection)
            {
                isConection = false;
            }
            //No connection
        }
        else if (!HtmlText.Contains("schema.org/WebPage"))
        {
            //Redirecting since the beginning of googles html contains that 
            //phrase and it was not found
        }
        else
        {
            if(!isConection)
            {
                isConection = true;

                MobileAds.Initialize(initStatus => { });
                if (!UserData.instance.isNoAds)
                {
                    this.RequestBanner();
                    RequestNativeAd();
                }
                RequestInterstitial();
                this.rewardBasedVideo = RewardBasedVideoAd.Instance;
                rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
                rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
                RequestRewardBasedVideo();
            }
            //success
        }
    }

    public void CheckConnect()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            if (isConection)
            {
                isConection = false;
            }
        }
        else
        {
            if (!isConection)
            {
                isConection = true;

                MobileAds.Initialize(initStatus => { });
                if (!UserData.instance.isNoAds)
                {
                    this.RequestBanner();
                    RequestNativeAd();
                }
                RequestInterstitial();
                this.rewardBasedVideo = RewardBasedVideoAd.Instance;
                rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
                rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
                RequestRewardBasedVideo();
            }
        }
    }
}
