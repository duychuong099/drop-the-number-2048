﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    public AudioClip click;
    public AudioClip merge;
    public AudioClip coin;
    public AudioClip topScore;
    public AudioClip revine;

    public AudioSource audioSource;

    public void SoundClick()
    {
        if (UserData.instance.isSound)
        {
            audioSource.PlayOneShot(click);
        }
    }
    public void SoundMerge()
    {
        if (UserData.instance.isSound)
        {
            audioSource.PlayOneShot(merge);
        }
    }
    public void SoundCoin()
    {
        if (UserData.instance.isSound)
        {
            audioSource.PlayOneShot(coin, 5);
        }
    }
    public void SoundTopScore()
    {
        if (UserData.instance.isSound)
        {
            audioSource.PlayOneShot(topScore);
        }
    }

    public void SoundRevine()
    {
        if (UserData.instance.isSound)
        {
            audioSource.PlayOneShot(revine);
        }
    }
}
