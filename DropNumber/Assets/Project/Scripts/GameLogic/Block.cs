﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    public float value { get; set; } = -1;
    public SpriteRenderer spriteRenderer;
    public int positionX = -1;
    public int positionY = -1;
    //public GameObject effect3;
    public GameObject gold;

    private void Start()
    {
        
    }

    private void Update()
    {
        if(Hammer.instance.isClicked2)
        {
            GetComponent<BoxCollider2D>().enabled = true;
        }
        else GetComponent<BoxCollider2D>().enabled = false;
    }

    public void setInfo(float typeValue)
    {
        value = typeValue;
        spriteRenderer.sprite = ReferenceManager.instance.numberSprite[(int)typeValue];
    }
    public void setPosition(int x, int y)
    {
        positionX = x;
        positionY = y;
    }
    public void DropRow()
    {
        positionY -= 1;
    }
    void OnMouseDown()
    {
        if (Hammer.instance.isClicked2 && positionX != -1 && gameObject != GameController.instance.currentBlock)
        {
            //GameController.instance.canHammer = false;
            Hammer.instance.DeActiveHammer();
            GameController.instance.Pause();
            //GameController.instance.board.block[positionX, positionY] = null;
            GameController.instance.AddDropQueue(positionX, positionY);
            if (GameController.instance.dropQueue.Count > 0)
            {
                GameController.instance.DropColumn();
                GameController.instance.isNotSpawn = true;
            }
            else
            {
                GameController.instance.Pause();
            }
            UserData.instance.numHammer -= 1;
            UserData.instance.Save();
            GameController.instance.ReDotweenAfterHammer();
            if (!GameController.instance.isNotSpawn)
            {
                GameController.instance.slowMove.TogglePause();
            }
            Destroy(this.gameObject);
        }
    }

    public void Effect3()
    {
        
    }
}
