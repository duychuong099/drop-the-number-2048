﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;

public class Board : MonoBehaviour
{
    public int maxRow = 7;
    public Block[,] block = new Block[5,7];

    void Start()
    {
        
    }

    public int getPositionDrop(int line)
    {
        for (int i=0; i<maxRow;i++)
        {
            if (block[line, i] == null || block[line, i].value == -1) return i;
        }
        return maxRow - 1;
    }
}
