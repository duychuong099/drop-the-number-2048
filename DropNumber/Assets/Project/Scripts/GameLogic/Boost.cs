﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Boost : MonoBehaviour
{
    public static Boost instance;

    private void Awake()
    {
        instance = this;
    }

    public GameObject Popup500;
    public GameObject PopupPreview;

    Animator areaAnim;
    public GameObject area;
    public GameObject gold;
    public GameObject freeVideo;

    public void OnGoldButtonPreview()
    {
        if (UserData.instance.gold < 100) return;
        DisableButton();
        AreaAnim();
        UiController.instance.GoldMinus();
        UiController.instance.isBoostPreview = true;
    }
    public void OnVideoButtonPreview()
    {
        //ads
        if (AdsManager.instance.isRewardAvailable())
        {
            AdsManager.instance.TypeReward = "VideoPreview";
            AdsManager.instance.ShowAdsRewards();         
        }
        else
        {
            Debug.Log("No ads available");
            SceneManager.LoadScene("PopUpAds", LoadSceneMode.Additive);
        }
    }
    public void VideoPreview()
    {
        DisableButton();
        AreaAnim();
        UiController.instance.isBoostPreview = true;
    }
    public void OnGoldButton512()
    {
        if (UserData.instance.gold < 100) return;
        DisableButton();
        AreaAnim();
        UiController.instance.GoldMinus();
        UiController.instance.isBoost512 = true;
    }

    public void Video500()
    {
        DisableButton();
        AreaAnim();
        UiController.instance.isBoost512 = true;
    }
    public void OnVideoButton512()
    {
        //ads
        if (AdsManager.instance.isRewardAvailable())
        {
            AdsManager.instance.TypeReward = "Video500";
            AdsManager.instance.ShowAdsRewards();          
        }
        else
        {
            Debug.Log("No ads available");
            SceneManager.LoadScene("PopUpAds", LoadSceneMode.Additive);
        }
    }

    public void AreaAnim()
    {
        area.SetActive(true);
        area.transform.DOScale(0.75f, 0.4f);
        area.GetComponent<Image>().DOFade(255f, 0.4f);
    }

    void DisableButton()
    {
        gold.SetActive(false);
        freeVideo.SetActive(false);
    }
}
