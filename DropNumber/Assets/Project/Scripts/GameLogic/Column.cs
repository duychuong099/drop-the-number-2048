﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Column : MonoBehaviour
{
    public int id;

    private void Update()
    {
        if (Hammer.instance.isClicked2)
        {
            GetComponent<BoxCollider2D>().enabled = false;
        }
        else GetComponent<BoxCollider2D>().enabled = true;
    }
    void OnMouseDown()
    {
        if (!Hammer.instance.isClicked1 && !Hammer.instance.isClicked2)
        {
            GameController.instance.SwitchLine(id);
        }
        if(Hammer.instance.isClicked1)
        {
            //GameController.instance.canHammer = false;
            Hammer.instance.DeActiveHammerColor();
            GameController.instance.Pause();
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    if (GameController.instance.board.block[i, j] != null)
                    {
                        if (GameController.instance.board.block[i, j].value == Hammer.instance.valueColorHammer)
                        {
                            Destroy(GameController.instance.board.block[i, j].gameObject);
                            GameController.instance.board.block[i, j] = null;
                        }
                    }
                }
            }
            GameController.instance.AllDrop();
            if (GameController.instance.isAllDrop)
            {
                GameController.instance.isNotSpawn = true;
            }
            else
            {
                GameController.instance.Pause();
            }
            UserData.instance.numColorHammer -= 1;
            UserData.instance.Save();
        }
    }

    private void OnMouseUp()
    {
        if (!Hammer.instance.isClicked1 && !Hammer.instance.isClicked2 && GameController.instance.isSwitchColumn)
        {
            GameController.instance.SpeedUp();
            GameController.instance.isSwitchColumn = false;
        }
    }
    private void OnMouseOver()
    {
        if (!Hammer.instance.isClicked1 && !Hammer.instance.isClicked2)
        {
            if(GameController.instance.isSwitchColumn) GameController.instance.SwitchLine(id);
        }
    }
}
