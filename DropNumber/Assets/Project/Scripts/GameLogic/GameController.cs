﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
using TMPro;
using System.Security.Cryptography;

public class GameController : MonoBehaviour
{
    public static GameController instance;

    private void Awake()
    {
        instance = this;
        Input.multiTouchEnabled = false;
    }

    public Animator gameAnim;
    public Transform[] lineSpawn;
    public Transform blockContain;
    public Board board;
    public GameObject Revive;
    public GameObject TopScorePopup;
    public GameObject blockPrefab;
    public GameObject PopUpBoost;
    public GameObject ActivePauseGO;
    public GameObject PiggyBank;
    public GameObject redline;
    public GameObject effect1;
    public GameObject effect2;
    public Block[][] boardReference;
    public SpriteRenderer[] speedUpEffect;
    public TextMeshProUGUI textScorePopup;
    public bool isGameOver { get; set; } = false;
    public bool isPause { get; set; } = false;
    public int currentScore { get; set; }
    public bool isTopScore { get; set; } = false;
    public bool isNotSpawn { get; set; } = false;
    public bool isAllDrop { get; set; } = false;
    public bool isPopupCongrat { get; set; } = false;
    public bool isSwitchColumn { get; set; } = false;
    public bool canHammer { get; set; } = false;
    public bool isTap { get; set; } = false;
    public int ValueCongrat { get; set; }

    [SerializeField] float blockSpeed = 1;
    [SerializeField] float fastSpeed = 15;

    public GameObject currentBlock { get; set; }
    public Sequence slowMove { get; set; }
    private Sequence combineMove;
    private Sequence dropMove;
    private Sequence speedUpMove;
    public float posMove;
    private float blockLength = 1.88f;
    private int currentLine;
    private int lastLine;
    private int numberValue;
    private bool canSpeedUp = false;
    private Queue<Block> blockQueue = new Queue<Block>();
    private bool[] congrat = {false, false, false, false, false, false, false, false, false };
    public Queue<Block> dropQueue { get; set; } = new Queue<Block>();
    public Queue<Block> effectQueue { get; set; } = new Queue<Block>();
    public AudioManager audioManager;
    public GameObject camera;
    public GameObject midGroup;
    public GameObject canvas;
    public ActivePiggy activePiggy;
    public GameObject adsfree;

    private void Start()
    {
        currentLine = 2;
        currentScore = 0;
        StartCoroutine(StartGame());
    }
    private void Update()
    {
    
    }

    IEnumerator StartGame()
    {
        if(!UserData.instance.isPlaying)
        {
            UserData.instance.isPlaying = true;
            UserData.instance.Save();
            gameAnim.enabled = true;
            yield return new WaitForSeconds(gameAnim.GetCurrentAnimatorStateInfo(0).length);
            PopUpBoost.SetActive(true);
            //PopUpBoost.GetComponent<Animator>().enabled = true;
        }
        else
        {
            UserData.instance.isPlaying = true;
            UserData.instance.Save();
            gameAnim.enabled = true;
            yield return new WaitForSeconds(gameAnim.GetCurrentAnimatorStateInfo(0).length);
            PopUpBoost.SetActive(true);
            //PopUpBoost.GetComponent<Animator>().enabled = true;
        }
    }

    public void Spawn(int value)
    {
        canSpeedUp = true;
        if (currentBlock != null)
        {
            currentBlock.GetComponent<Block>().spriteRenderer.sortingOrder = 0;
        }
        currentBlock = Instantiate(blockPrefab, blockContain.transform);
        if (Random.Range(0, 100) > 95) currentBlock.GetComponent<Block>().gold.SetActive(true);
        if (value == -1)
        {
            numberValue = Random.Range(0, 5);
        }
        else
        {
            numberValue = value;
            if (UiController.instance.isBoostPreview)
            {
                UiController.instance.nextValue = Random.Range(0, 5);
                //UiController.instance.nextValue = Random.Range(8, 8);
                UiController.instance.blockPreview.GetComponent<Block>().setInfo(UiController.instance.nextValue);
            }
        }

        currentBlock.GetComponent<Block>().setInfo(numberValue);
        currentBlock.transform.position = lineSpawn[currentLine].transform.position;
        currentBlock.GetComponent<Block>().spriteRenderer.sortingOrder = 1;
        posMove = (float)board.getPositionDrop(currentLine) * blockLength - 6;
        scaleAnimation();
    }
    public void scaleAnimation()
    {
        canHammer = true;
        currentBlock.transform.localScale = new Vector3(0, 0, 0);
        slowMove = DOTween.Sequence();
        slowMove.Append(currentBlock.transform.DOScale(1.1f, 0.25f));
        slowMove.Append(currentBlock.transform.DOScale(1f, 0.05f));
        slowMove.AppendInterval(0.1f);
        
        float speed = blockSpeed;
        if (currentScore >= 200 && currentScore < 500) speed *= 1.1f;
        else if (currentScore >= 500 && currentScore < 1000) speed *= 1.3f;
        else if (currentScore >= 1000 && currentScore < 2000) speed *= 1.5f;
        else if (currentScore >= 2000) speed *= 1.7f;

        slowMove.Append(currentBlock.transform.DOLocalMoveY(posMove, Mathf.Abs(currentBlock.transform.position.y - posMove) / speed).SetEase(Ease.Linear).OnComplete(() =>
        {
            Effect1(currentBlock.GetComponent<Block>());
            SaveToBoard();
            nextBlock();
        }));
    }

    public void SwitchLine(int lineId)
    {
        if(canSpeedUp && !isGameOver && !isPause)
        {
            canHammer = false;
            float highestPos = -1000;
            if (lineId > currentLine)
            {
                for (int line = currentLine + 1; line <= lineId; line++)
                {
                    for (int i = 4; i >= 0; i--)
                    {
                        if (board.block[line, i] != null)
                        {
                            if (board.block[line, i].transform.position.y + blockLength > highestPos)
                            {
                                highestPos = board.block[line, i].transform.position.y + blockLength;
                                break;
                            }
                        }
                    }
                }
            }
            else if (lineId < currentLine)
            {
                for (int line = currentLine - 1; line >= lineId; line--)
                {
                    for (int i = 4; i >= 0; i--)
                    {
                        if (board.block[line, i] != null)
                        {
                            if (board.block[line, i].transform.position.y + blockLength > highestPos)
                            {
                                highestPos = board.block[line, i].transform.position.y + blockLength;
                                break;
                            }
                        }
                    }
                }
            }
            if (currentBlock.transform.position.y > highestPos)
            {
                isSwitchColumn = true;
                if (currentBlock != null)
                {
                    currentBlock.transform.position = new Vector3(lineSpawn[lineId].position.x, currentBlock.transform.position.y, 0);
                    currentBlock.transform.localScale = new Vector3(1, 1, 1);

                    if (lineId != currentLine)
                    {
                        currentLine = lineId;
                        //Debug.Log("ok1");
                        float speed = blockSpeed;
                        if (currentScore >= 200 && currentScore < 500) speed *= 1.1f;
                        else if (currentScore >= 500 && currentScore < 1000) speed *= 1.3f;
                        else if (currentScore >= 1000 && currentScore < 2000) speed *= 1.5f;
                        else if (currentScore >= 2000) speed *= 1.7f;

                        posMove = (float)board.getPositionDrop(lineId) * blockLength - 6;
                        slowMove.Kill();
                        slowMove = DOTween.Sequence();
                        slowMove.Append(currentBlock.transform.DOLocalMoveY(posMove, Mathf.Abs(currentBlock.transform.position.y - posMove) / speed).SetEase(Ease.Linear).OnComplete(() =>
                        {
                            //Debug.Log("ok");
                            Effect1(currentBlock.GetComponent<Block>());
                            SaveToBoard();
                            nextBlock();
                        }));
                    }
                }
            }
        }
    }

    public void SpeedUp()
    {
        if (canSpeedUp && !isGameOver && !isPause)
        {
            canHammer = false;
            canSpeedUp = false;
            currentBlock.transform.localScale = new Vector3(1, 1, 1);
            slowMove.Kill();
            //if (currentBlock != null)
            //{
                //currentBlock.transform.position = new Vector3(lineSpawn[lineId].position.x, currentBlock.transform.position.y, 0);
                //currentBlock.transform.localScale = new Vector3(1, 1, 1);
            //}
            posMove = board.getPositionDrop(currentLine) * blockLength - 6;
            if (speedUpEffect[currentLine] != null)
            {
                if (currentBlock.GetComponent<Block>().value != 8)
                {
                    speedUpEffect[currentLine].color = ReferenceManager.instance.effectSpeedUp[(int)currentBlock.GetComponent<Block>().value];
                }
                else speedUpEffect[currentLine].color = ReferenceManager.instance.effectSpeedUp[5];
            }
            
            speedUpMove = DOTween.Sequence();
            float time = Mathf.Abs(currentBlock.transform.position.y - posMove) /fastSpeed;
            speedUpMove.Insert(0, currentBlock.transform.DOLocalMoveY(posMove, time).SetEase(Ease.Linear));
            speedUpMove.Insert(0, speedUpEffect[currentLine].DOFade(0.35f,time/2));
            speedUpMove.Insert(2*time/3, speedUpEffect[currentLine].DOFade(0, time / 3));
            speedUpMove.OnComplete(() => {
                Effect1(currentBlock.GetComponent<Block>());
                SaveToBoard();
                nextBlock();
            });
        }
    }

    public void SaveToBoard()
    {
        isSwitchColumn = false;
        int y = board.getPositionDrop(currentLine);
        board.block[currentLine, y] = currentBlock.GetComponent<Block>();
        board.block[currentLine, y].setPosition(currentLine, y);
    }
    public void nextBlock()
    {
        blockQueue.Enqueue(currentBlock.GetComponent<Block>());
        CheckCombineBlock();
    }
    void CheckCombineBlock()
    {
        Block blockCheck = blockQueue.Peek();
        int x = blockCheck.positionX;
        int y = blockCheck.positionY;
        bool isCombineLeft = false;
        bool isCombineRight = false;
        bool isCombineUnder = false;
        int numCombine = 0;
        if (x > 0)
        {
            if (board.block[x - 1, y] != null && blockCheck!=null) {
                if (blockCheck.value == board.block[x - 1, y].value)
                {
                    isCombineLeft = true;
                    numCombine += 1;
                }
            }
        }
        if (x < 4)
        {
            if (board.block[x + 1, y] != null && blockCheck != null)
            {
                if (blockCheck.value == board.block[x + 1, y].value)
                {
                    isCombineRight = true;
                    numCombine += 1;
                }
            }
        }
        if (y > 0)
        {
            if (board.block[x, y -1] != null && blockCheck != null)
            {
                if (blockCheck.value == board.block[x, y -1].value)
                {                  
                    isCombineUnder = true;
                    numCombine += 1;
                }
            }
        }

        if(numCombine > 0 && board.block[x, y] != null && board.block[x, y].value != -1)
        {
            StartCoroutine(CombineBlock(x, y, isCombineLeft, isCombineRight, isCombineUnder, numCombine));
        }
        else
        {
            blockQueue.Dequeue();
            if (blockQueue.Count == 0)
            {
                CheckGameOver();
                if (!isGameOver)
                {
                    if (!isNotSpawn)
                    {
                        if (isPopupCongrat)
                        {
                            UiController.instance.ActivePopupCongrat();
                        }
                        else
                        {
                            Spawn(UiController.instance.nextValue);
                        }
                    }
                    else
                    {
                        //hammer
                        isNotSpawn = false;
                        
                        GameController.instance.canHammer = true;
                        Pause();
                    }
                }
                else
                {
                    Debug.Log("GameOver");
                    CaptureIt();
                    StartCoroutine(GameOver());
                }
            }
            else CheckCombineBlock();
        }
    }
    public IEnumerator CoCaptureIt()
    {
        yield return new WaitForEndOfFrame();
        Debug.Log("Captured");
        UserData.instance.isShowCapture = true;
        string fileName = "DropTheNumberResult.png";
        ScreenCapture.CaptureScreenshot(fileName);
    }

    public void CaptureIt()
    {
        StartCoroutine(CoCaptureIt());
    }
    IEnumerator CombineBlock(int x, int y, bool isCombineLeft, bool isCombineRight, bool isCombineUnder, int numCombine)
    {
        yield return new WaitForSeconds(0.05f);
        board.block[x, y].GetComponent<Block>().spriteRenderer.sortingOrder = 1;
        if (board.block[x, y] != null && board.block[x, y].value != -1)
        {
            int blockCombineValue = (int)board.block[x, y].value + numCombine;
            if(blockCombineValue >= 9)
            {
                if(!congrat[blockCombineValue-9])
                {
                    congrat[blockCombineValue - 9] = true;
                    isPopupCongrat = true;
                    ValueCongrat = blockCombineValue;
                }
            }
            board.block[x, y].setInfo(blockCombineValue);
            bool isGoldPiggy = false;
            if (board.block[x, y].gold.activeSelf)
            {
                board.block[x, y].gold.SetActive(false);
                isGoldPiggy = true;
            }
            currentScore += (int)Mathf.Pow(2, blockCombineValue + 1);
            if (blockCombineValue >= 9)
            {
                UserData.instance.blockNum[blockCombineValue - 9] += 1;
                UserData.instance.Save();
            }

            if (currentScore > UserData.instance.topScore)
            {
                isTopScore = true;
                UiController.instance.SetTopScore(currentScore);
            }
            UiController.instance.SetScore(currentScore);
            combineMove = DOTween.Sequence();
            if (isCombineUnder)
            {
                audioManager.SoundMerge();
                combineMove.Insert(0, board.block[x, y - 1].transform.DOMoveY(board.block[x, y].transform.position.y, 0.25f));
                board.block[x, y - 1].Effect3();
                if(board.block[x, y - 1].gold.activeSelf)
                {
                    board.block[x, y - 1].gold.SetActive(false);
                    isGoldPiggy = true;
                }
            }
            if (isCombineLeft)
            {
                audioManager.SoundMerge();
                combineMove.Insert(0, board.block[x - 1, y].transform.DOMoveX(board.block[x, y].transform.position.x, 0.25f));
                board.block[x - 1, y].Effect3();
                if (board.block[x - 1, y].gold.activeSelf)
                {
                    board.block[x - 1, y].gold.SetActive(false);
                    isGoldPiggy = true;
                }
            }
            if (isCombineRight)
            {
                audioManager.SoundMerge();
                combineMove.Insert(0, board.block[x + 1, y].transform.DOMoveX(board.block[x, y].transform.position.x, 0.25f));
                board.block[x + 1, y].Effect3();
                if (board.block[x + 1, y].gold.activeSelf)
                {
                    board.block[x + 1, y].gold.SetActive(false);
                    isGoldPiggy = true;
                }
            }
            combineMove.OnComplete(() =>
            {
                GameEffect.instance.ShowEffectScore(board.block[x, y].transform.position, board.block[x, y].value);
                board.block[x, y].GetComponent<Block>().spriteRenderer.sortingOrder = 0;
                if (isCombineUnder)
                {
                    Destroy(board.block[x, y - 1].gameObject);
                    AddDropQueue(x, y - 1);
                }
                if (isCombineLeft)
                {
                    Destroy(board.block[x - 1, y].gameObject);
                    AddDropQueue(x - 1, y);
                }
                if (isCombineRight)
                {
                    Destroy(board.block[x + 1, y].gameObject);
                    AddDropQueue(x + 1, y);
                }
                if(isGoldPiggy)
                {
                    PigMove();
                }
                DropColumn();
            });
        }
    }
    public void PigMove()
    {
        //Pause();
        GameObject gold = Instantiate(PiggyBank.GetComponent<Piggy>().gold, PiggyBank.transform);
        gold.SetActive(true);
        gold.transform.position = PiggyBank.GetComponent<Piggy>().start.position;
        gold.transform.localScale = new Vector3(0, 0, 0);
        Sequence pigMove = DOTween.Sequence();
        pigMove.Append(gold.transform.DOScale(1f, 0.3f));
        pigMove.Append(gold.transform.DOScale(0.9f, 0.15f));
        pigMove.AppendInterval(.45f);
        pigMove.Insert(0.9f, DOTweenModulePhysics.DOMove(gold.GetComponent<Rigidbody>(), PiggyBank.GetComponent<Piggy>().des.position, 0.5f));
        //pigMove.Insert(0.7f, gold.transform.DOMove(gold.GetComponent<Rigidbody2D>(), PiggyBank.GetComponent<Piggy>().des.position, 0.5f));
        pigMove.Insert(0.9f, gold.transform.DOScale(0.2f, 0.5f));

        pigMove.OnComplete(() => {
            //Pause();
            Destroy(gold);
            UserData.instance.Save();
            activePiggy.Active();
        });
    }

    public void AddDropQueue(int x, int y)
    {
        int i = y + 1;
        while(board.block[x, i] != null && i < board.maxRow)
        {
            board.block[x, i - 1] = board.block[x, i];
            board.block[x, i - 1].DropRow();
            dropQueue.Enqueue(board.block[x, i - 1]);
            blockQueue.Enqueue(board.block[x, i - 1]);
            i++;
        }
        if (i < board.maxRow)
        {
            board.block[x, i - 1] = null;
        }
    }

    public void DropColumn()
    {
        bool check = false;
        dropMove = DOTween.Sequence();
        while (dropQueue.Count != 0)
        {
            check = true;
            Block x = dropQueue.Dequeue();
            effectQueue.Enqueue(x);
            dropMove.Insert(0, x.transform.DOLocalMoveY(x.transform.localPosition.y - blockLength, 0.25f));
        }
        if (check)
        {
            dropMove.OnComplete(() =>
            {
                while(effectQueue.Count > 0)
                {
                   Block x = effectQueue.Dequeue();
                   Effect1(x);
                }
                CheckCombineBlock();
            });
        }
        else CheckCombineBlock();
    }

    public void AllDrop()
    {
        bool check = false;
        dropMove = DOTween.Sequence();
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                if (board.block[i,j] == null || board.block[i, j].value == -1)
                {
                    for (int k = j+1; k < 6; k++)
                    {
                        if (board.block[i, k] != null && board.block[i, k].value != -1)
                        {
                            check = true;
                            board.block[i, j] = board.block[i, k];
                            board.block[i, k] = null;
                            dropMove.Insert(0, board.block[i, j].transform.DOLocalMoveY(board.block[i, j].transform.localPosition.y - blockLength*(k- j), 0.25f * (k - j)));
                            board.block[i, j].positionY -= (k - j);
                            blockQueue.Enqueue(board.block[i, j]);
                            effectQueue.Enqueue(board.block[i, j]);
                            break;
                        }
                    }
                }
            }
        }
        ReDotweenAfterHammer();
        if (check)
        {
            isAllDrop = true;
            dropMove.OnComplete(() =>
            {
                while (effectQueue.Count > 0)
                {
                    Block x = effectQueue.Dequeue();
                    Effect1(x);
                }
                isAllDrop = false;
                CheckCombineBlock();
            });
        }
        //else CheckCombineBlock();
    }

    void CheckGameOver()
    {
        for(int i=0;i<5;i++)
        {
            if(board.block[i,5]!=null)
            {
                isGameOver = true;
                return;
            }
        }
    }
    IEnumerator GameOver()
    {
        redline.SetActive(true);
        redline.GetComponent<SpriteRenderer>().DOFade(0, 0.25f).OnComplete(() =>
        {
            redline.GetComponent<SpriteRenderer>().DOFade(1, 0.25f).OnComplete(() =>
            {
                redline.GetComponent<SpriteRenderer>().DOFade(0, 0.25f).OnComplete(() =>
                {
                    redline.GetComponent<SpriteRenderer>().DOFade(1, 0.25f).OnComplete(() =>
                    {
                        redline.SetActive(false);
                    });
                });
            });
        });
        yield return new WaitForSeconds(1f);
        Revive.SetActive(true);
        audioManager.SoundRevine();
    }

    public void Pause()
    {
        if(!isPause)
        {
            isPause = true;
        }
        else
        {
            isPause = false;
        }
        slowMove.TogglePause();
        speedUpMove.TogglePause();
        //dropMove.TogglePause();
        //combineMove.TogglePause();
    }
    public void ReviveAction()
    {
        StartCoroutine(CoRevive());
    }
    IEnumerator CoRevive()
    {
        isGameOver = false;
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < 5; i++)
        {
            for (int j = 3; j <= 5; j++)
            {
                if (board.block[i, j] != null)
                    Destroy(board.block[i, j].gameObject);
            }
        }
        yield return new WaitForSeconds(0.25f);
        Spawn(UiController.instance.nextValue);
    }

    public void ActivetopScore()
    {
        TopScorePopup.SetActive(true);
        textScorePopup.text = currentScore.ToString();
        audioManager.SoundTopScore();
        UiController.instance.ActiveConfetti();
    }
    public void OnButtonCalm50()
    {
        UserData.instance.gold += 50;
        UserData.instance.Save();
        FinishPlayed();
        //AudioManager.instance.SoundClick();
        UserData.instance.isResultCoinMove = true;
        SceneManager.LoadScene("GameResult");       
    }
    public void OnButtonVideo150()
    {
        //ads
        if (AdsManager.instance.isRewardAvailable())
        {
            AdsManager.instance.TypeReward = "Claim150";
            AdsManager.instance.ShowAdsRewards();
            
        }
    }

    public void Claim150()
    {
        UserData.instance.gold += 150;
        UserData.instance.Save();
        FinishPlayed();
        //AudioManager.instance.SoundClick();
        UserData.instance.isResultCoinMove = true;
        SceneManager.LoadScene("GameResult");
    }


    public void FinishPlayed()
    {
        UserData.instance.gamePlayed += 1;
        UserData.instance.Save();
    }

    public void OnButtonPause()
    {
        if (!UiController.instance.isRateStar)
        {
            Pause();
            
            SceneManager.LoadScene("Pause", LoadSceneMode.Additive);
        }
        else
        {
            UiController.instance.ActiveRateStar();
        }
    }
    public void DeActiveCamera()
    {
        camera.SetActive(false);
        midGroup.SetActive(false);
        canvas.SetActive(false);
    }

    public void ActiveCamera()
    {
        camera.SetActive(true);
        midGroup.SetActive(true);
        canvas.SetActive(true);
    }

    public void ActivePause()
    {
        ActivePauseGO.SetActive(true);
    }
    public void ActivePauseButton()
    {
        ActivePauseGO.SetActive(false);
        Pause();
    }

    void Effect1(Block block)
    {
        GameObject effect = Instantiate(effect1, board.transform);
        int effect2Length = 15;
        GameObject[] eff2 = new GameObject[effect2Length];
        effect.transform.position = block.transform.position;
        Sequence effect2Move = DOTween.Sequence();
        for (int i = 0; i < effect2Length; i++)
        {
            eff2[i] = Instantiate(effect2, effect.transform);
            eff2[i].transform.position = effect.transform.position + new Vector3(-0.7f + i * 1.4f / effect2Length, -0.7f, 0);
            float x = Random.Range(0.6f, 1.3f);
            eff2[i].transform.localScale = new Vector3(x,x,x);
            effect2Move.Insert(0, eff2[i].transform.DOMoveY(eff2[i].transform.position.y + Random.Range(1f, 2.7f), 1f));
            effect2Move.Insert(0.5f, eff2[i].GetComponent<SpriteRenderer>().DOFade(0, 0.5f));
        }

        effect2Move.Insert(0.5f, effect.GetComponentInChildren<SpriteRenderer>().DOFade(0f, 0.5f));
        effect2Move.OnComplete(() =>
        {
            Destroy(effect);
        });
    }

    public void ReDotweenAfterHammer()
    {
        float speed = blockSpeed;
        if (currentScore >= 200 && currentScore < 500) speed *= 1.1f;
        else if (currentScore >= 500 && currentScore < 1000) speed *= 1.3f;
        else if (currentScore >= 1000 && currentScore < 2000) speed *= 1.5f;
        else if (currentScore >= 2000) speed *= 1.7f;

        posMove = (float)board.getPositionDrop(currentLine) * blockLength - 6;
        Debug.Log(board.getPositionDrop(currentLine));
        slowMove.Kill();
        slowMove = DOTween.Sequence();
        slowMove.Append(currentBlock.transform.DOLocalMoveY(posMove, Mathf.Abs(currentBlock.transform.position.y - posMove) / speed).SetEase(Ease.Linear).OnComplete(() =>
        {
            //Debug.Log("ok");
            Effect1(currentBlock.GetComponent<Block>());
            SaveToBoard();
            nextBlock();
        }));
        
        slowMove.TogglePause();
    }
}
