﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class Hammer : MonoBehaviour
{
    public static Hammer instance;

    private void Awake()
    {
        instance = this;
    }

    public TextMeshProUGUI numHammer1text;
    public TextMeshProUGUI numHammer2text;
    public Image blockImage;
    public Image blockImagePopup;

    public GameObject Hammer1free;
    public GameObject Hammer1stock;
    public GameObject Hammer2free;
    public GameObject Hammer2stock;

    public GameObject HammerPopup;
    public GameObject HammerPopup1;
    public GameObject HammerPopup2;

    public GameObject topGroup1;
    public GameObject topGroup2;

    public bool isClicked1 { get; set; } = false;
    public bool isClicked2 { get; set; } = false;
    public bool isHammer1 { get; set; } = false;
    public bool isHammer2 { get; set; } = false;
    public int valueColorHammer;

    float timer;

    private void Start()
    {
        UpdateUi();
        UpdateValueColor();
    }

    private void Update()
    {
        UpdateUi();
        timer += Time.deltaTime;
        if (timer > 15 && !isClicked1)
        {
            UpdateValueColor();
        }
    }

    void UpdateUi()
    {
        if (UserData.instance.numColorHammer > 0)
        {
            numHammer1text.text = UserData.instance.numColorHammer.ToString();
            Hammer1stock.SetActive(true);
            Hammer1free.SetActive(false);
        }
        else
        {
            Hammer1stock.SetActive(false);
            Hammer1free.SetActive(true);
        }

        if (UserData.instance.numHammer > 0)
        {
            numHammer2text.text = UserData.instance.numHammer.ToString();
            Hammer2stock.SetActive(true);
            Hammer2free.SetActive(false);
        }
        else
        {
            Hammer2stock.SetActive(false);
            Hammer2free.SetActive(true);
        }
    }

    public void OnButtonHammerColor()
    {
        if (GameController.instance.canHammer)
        {
            if (UiController.instance.isRateStar)
            {
                UiController.instance.ActiveRateStar();
            }
            else if (UserData.instance.numColorHammer > 0)
            {
                if (!isClicked1)
                {
                    isClicked1 = true;
                    HammerPopup.SetActive(true);
                    HammerPopup1.SetActive(true);
                    HammerPopup2.SetActive(false);
                    topGroup1.SetActive(false);
                    topGroup2.SetActive(false);
                    HammerPopup1.transform.localPosition = new Vector3(0, 300, 0);
                    HammerPopup1.transform.DOLocalMoveY(245, 0.5f);
                    if (!isClicked2)
                    {
                        GameController.instance.Pause();
                    }
                    else isClicked2 = false;
                }
                else
                {
                    DeActiveHammerColor();
                }
            }
            else
            {
                isHammer1 = true;
                isHammer2 = false;
                SceneManager.LoadScene("Hammer", LoadSceneMode.Additive);
                GameController.instance.Pause();
            }
        }
    }

    public void DeActiveHammerColor()
    {
        isClicked1 = false;
        HammerPopup.SetActive(false);
        HammerPopup1.SetActive(false);
        topGroup1.SetActive(true);
        topGroup2.SetActive(true);
        GameController.instance.Pause();
    }
    public void OnButtonHammer()
    {
        if (GameController.instance.canHammer)
        {
            if (UiController.instance.isRateStar)
            {
                UiController.instance.ActiveRateStar();
            }
            else if (UserData.instance.numHammer > 0)
            {
                if (!isClicked2)
                {
                    isClicked2 = true;
                    HammerPopup.SetActive(true);
                    HammerPopup2.SetActive(true);
                    HammerPopup1.SetActive(false);
                    topGroup1.SetActive(false);
                    topGroup2.SetActive(false);
                    HammerPopup2.transform.localPosition = new Vector3(0, 300, 0);
                    HammerPopup2.transform.DOLocalMoveY(245, 0.5f);
                    if (!isClicked1)
                    {
                        GameController.instance.Pause();
                    }
                    else isClicked1 = false;
                }
                else
                {
                    DeActiveHammer();
                }
            }
            else
            {
                isHammer2 = true;
                isHammer1 = false;
                SceneManager.LoadScene("Hammer", LoadSceneMode.Additive);
                GameController.instance.Pause();
            }
        }
    }

    public void DeActiveHammer()
    {
        isClicked2 = false;
        HammerPopup.SetActive(false);
        HammerPopup2.SetActive(false);
        topGroup1.SetActive(true);
        topGroup2.SetActive(true);
        GameController.instance.Pause();
    }

    public void UpdateValueColor()
    {
        valueColorHammer = Random.Range(0, 4);
        blockImage.GetComponent<Image>().sprite = ReferenceManager.instance.numberSprite[valueColorHammer];
        blockImagePopup.GetComponent<Image>().sprite = ReferenceManager.instance.numberSprite[valueColorHammer];
        timer = 0;
    }
}
