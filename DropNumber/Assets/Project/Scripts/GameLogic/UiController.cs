﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UiController : MonoBehaviour
{
    public Animator boostAnim;
    public static UiController instance;
    private void Awake()
    {
        instance = this;
    }
    public GameObject block;
    //public GameObject block512;
    public GameObject blockPreview { get; set; }
    public GameObject rateFiveStarPopup;
    public GameObject popupCongrat;

    public Transform blockPreviewTransform;

    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI goldText;
    public TextMeshProUGUI topScoreText;
    public TextMeshProUGUI rankText;
    public TextMeshProUGUI congratText;
    public Image blockCongrat;
    public Image[] area;

    public int nextValue { get; set; } = -1;

    public bool isBoost512;
    public bool isBoostPreview;
    public bool isRateStar = false;
    public bool isSmallFreeButton = false;
    public bool isClaim30 = false;

    private float scoreValue = 0;
    private float timeRateStar;
    private bool isPreviewLater = false;

    public Button BoostStart;
    public Button Free500;
    public Button Gold500;
    public Button FreePreview;
    public Button GoldPreview;

    public Button SmallFreePreview;
    public Button SmallGoldPreview;

    public GameObject ConfettiEffect;
    public GameObject coinPrefab;

    public Transform coinDes;
    public Transform coin10start;
    public Transform coin30start;
    public Transform coinRateStart;

    private void Start()
    {
        SetRankText();
        UpdateUi();
        timeRateStar = 0;
    }

    private void Update()
    {
        UpdateScore();
        UpdateUi();
        timeRateStar += Time.deltaTime;
        if (PlayerPrefs.GetInt("Rated", 0) == 0)
        {
            if (timeRateStar > 300)
            {
                isRateStar = true;
            }
        }

        ActiveBoost();
        Claim30Active();
    }

    private void OnApplicationPause(bool pause)
    {
        if (UserData.instance.isNextPause && pause)
        {
            //add scene pause
            if (!GameController.instance.ActivePauseGO.activeSelf)
            {
                GameController.instance.Pause();
            }
            if (SceneManager.GetActiveScene().name != "Pause")
            {
                SceneManager.LoadScene("Pause", LoadSceneMode.Additive);
            }
        }
        UserData.instance.isNextPause = true;
    }

    public void ActiveRateStar()
    {
        if (PlayerPrefs.GetInt("Rated", 0) == 0)
        {
            GameController.instance.Pause();
            rateFiveStarPopup.SetActive(true);
            timeRateStar = 0;
            isRateStar = false;
        }
    }
    public void OnButtonRate()
    {
        Application.OpenURL("market://details?id=" + Application.identifier);
        rateFiveStarPopup.SetActive(false);
        PlayerPrefs.SetInt("Rated", 1);
        GameController.instance.ActivePause();
    }
    public void OnButtonLater()
    {
        rateFiveStarPopup.SetActive(false);
        GameController.instance.ActivePause();
    }
    public void OnButtonNever()
    {
        rateFiveStarPopup.SetActive(false);
        PlayerPrefs.SetInt("Rated", 1);
        GameController.instance.ActivePause();
    }

    public void SetScore(float score)
    {
        DOTween.To(x => scoreValue = x, scoreValue, score, 0.25f);
    }
    public void SetTopScore(float topscore)
    {
        DOTween.To(x => UserData.instance.topScore = (int)x, UserData.instance.topScore, topscore, 0.25f);
        UserData.instance.Save();
    }
    void UpdateScore()
    {
        scoreText.text = ((int)scoreValue).ToString();
    }

    public void OnButtonStart()
    {
        BoostStart.interactable = false;
        Free500.interactable = false; ;
        Gold500.interactable = false; ;
        FreePreview.interactable = false;
        GoldPreview.interactable = false;

        boostAnim.Play("PopUpBoostReversed");
        area[0].DOFade(0, 0.3f).SetEase(Ease.Linear);
        area[1].DOFade(0, 0.3f).SetEase(Ease.Linear);
        int valueStart = 0;
        if (isBoost512)
        {
            valueStart = 8; //512
        }
        if (isBoostPreview)
        {
            boostPreview();
        }
        else
        {
            GameController.instance.Spawn(valueStart);
        }

        if (!isBoostPreview)
        {
            SmallFreePreview.gameObject.SetActive(true);
            SmallGoldPreview.gameObject.SetActive(true);
        }
        GameController.instance.PopUpBoost.SetActive(false);
    }
    public void boostPreview()
    {
        if (isBoost512) nextValue = 8;
        else nextValue = Random.Range(0, 5);
        blockPreview = Instantiate<GameObject>(block, blockPreviewTransform);
        blockPreview.GetComponent<Block>().setInfo(nextValue);
        blockPreview.GetComponent<SpriteRenderer>().sortingOrder = 2;
        blockPreview.transform.position = new Vector3(0, 0, 0);
        blockPreview.transform.localScale = new Vector3(0, 0, 0);
        Sequence blockMove = DOTween.Sequence();
        blockMove.Append(blockPreview.transform.DOScale(1.1f, 0.25f));
        blockMove.Append(blockPreview.transform.DOScale(1f, 0.05f));
        blockMove.AppendInterval(0.1f);
        blockMove.Append(blockPreview.transform.DOMove(blockPreviewTransform.position, 1.5f).SetEase(Ease.Linear));
        blockMove.Insert(0.4f, blockPreview.transform.DOScale(0.45f, 1.5f).SetEase(Ease.Linear));
        blockMove.OnComplete(() =>
        {
            if (!isPreviewLater)
            {
                GameController.instance.Spawn(nextValue);
            }
            else
            {
                GameController.instance.Pause();
            }
        });
    }

    public void UpdateUi()
    {
        goldText.text = UserData.instance.gold.ToString();
        topScoreText.text = UserData.instance.topScore.ToString();
    }

    public void GoldMinus()
    {
        DOTween.To(x => UserData.instance.gold = Mathf.FloorToInt(x), UserData.instance.gold, UserData.instance.gold - 100, 0.25f).OnComplete(() =>
        {
            UserData.instance.Save();
        });
    }

    void SetRankText()
    {
        int number = UserData.instance.rank;
        if (number == 0)
        {
            rankText.text = "-";
        }
        else if (number % 10 == 1)
        {
            rankText.text = "#" + number.ToString() + "st";
        }
        else if (number % 10 == 2)
        {
            rankText.text = "#" + number.ToString() + "nd";
        }
        else if (number % 10 == 3)
        {
            rankText.text = "#" + number.ToString() + "rd";
        }
        else
        {
            rankText.text = "#" + number.ToString() + "th";
        }
        //rankText.text = # + UserData.instance.rank.ToString()
    }
    public void OnButtonRecord()
    {
        if (!UiController.instance.isRateStar)
        {
            GameController.instance.Pause();
            SceneManager.LoadScene("Record", LoadSceneMode.Additive);
        }
        else
        {
            UiController.instance.ActiveRateStar();
        }
    }

    public void OnButtonFree() //ads
    {
        if (UiController.instance.isRateStar)
        {
            UiController.instance.ActiveRateStar();
        }
        else if (AdsManager.instance.isRewardAvailable())
        {
            AdsManager.instance.TypeReward = "SmallFreePreviewButton";
            AdsManager.instance.ShowAdsRewards();
        }
        else
        {
            Debug.Log("No ads available");
            GameController.instance.Pause();
            PlayerPrefs.SetInt("isPopUpAdsPause", 1);
            SceneManager.LoadScene("PopUpAds", LoadSceneMode.Additive);
        }
    }

    public void SmallFreePreviewButton()
    {
        isSmallFreeButton = true;
    }

    void ActiveBoost()
    {
        if (isSmallFreeButton)
        {
            isSmallFreeButton = false;
            if (!isBoostPreview)
            {
                isPreviewLater = true;
                isBoostPreview = true;
                GameController.instance.Pause();
                isBoost512 = false;
                nextValue = Random.Range(0, 5);
                SmallFreePreview.gameObject.SetActive(false);
                SmallGoldPreview.gameObject.SetActive(false);
                boostPreview();
            }
        }
    }


    public void OnButton100()
    {
        if (UserData.instance.gold < 100) return;
        if (UiController.instance.isRateStar)
        {
            UiController.instance.ActiveRateStar();
        }
        else if (!isBoostPreview)
        {
            DOTween.To(x => UserData.instance.gold = Mathf.FloorToInt(x), UserData.instance.gold, UserData.instance.gold - 100, 0.25f).OnComplete(() =>
            {
                UserData.instance.Save();
            });
            isBoostPreview = true;
            UserData.instance.Save();
            isPreviewLater = true;
            GameController.instance.Pause();
            isBoost512 = false;
            nextValue = Random.Range(0, 5);
            SmallFreePreview.gameObject.SetActive(false);
            SmallGoldPreview.gameObject.SetActive(false);
            boostPreview();
        }
    }

    public void OnButtonGoldPlus()
    {
        if (!UiController.instance.isRateStar)
        {
            GameController.instance.Pause();
            PlayerPrefs.SetString("PrevScene", "Game");
            SceneManager.LoadScene("Shop", LoadSceneMode.Additive);
        }
        else
        {
            UiController.instance.ActiveRateStar();
        }
    }

    public void OnButtonPiggy()
    {
        if (UiController.instance.isRateStar)
        {
            UiController.instance.ActiveRateStar();
        }
        else if (!GameController.instance.activePiggy.isActive)
        {
            GameController.instance.Pause();
            PlayerPrefs.SetString("PrevScene", "Game");
            SceneManager.LoadScene("Piggy", LoadSceneMode.Additive);
        }
        else
        {
            if (AdsManager.instance.isRewardAvailable())
            {
                AdsManager.instance.TypeReward = "Piggy";
                AdsManager.instance.ShowAdsRewards();
            }
            else
            {
                Debug.Log("No ads available");
                GameController.instance.Pause();
                SceneManager.LoadScene("PopUpAds", LoadSceneMode.Additive);
            }
        }
    }

    public void ActivePiggy()
    {
        GameController.instance.activePiggy.DeActive(true);
    }

    public void ActivePopupCongrat()
    {
        popupCongrat.SetActive(true);
        GameController.instance.isPopupCongrat = false;
        blockCongrat.sprite = ReferenceManager.instance.numberSprite[GameController.instance.ValueCongrat];
        congratText.text = "You made " + Mathf.Pow(2, GameController.instance.ValueCongrat + 1).ToString() + "!";
        GameController.instance.audioManager.SoundTopScore();
        ActiveConfetti();
    }
    public void OnButtonClaim10()
    {
        AdsManager.instance.ShowAdsFullScreen();
        popupCongrat.SetActive(false);
        UserData.instance.gold += 10;
        UserData.instance.Save();
        GameController.instance.Spawn(nextValue);
        ConfettiEffect.SetActive(false);
        for (int i = 0; i < 10; i++)
        {
            StartCoroutine(CoinAnimate(i, coin10start, coinDes));
        }
    }

    IEnumerator CoinAnimate(int i, Transform moneySpawn, Transform moneyDes)
    {
        Sequence coinMove = DOTween.Sequence();
        float time = Random.Range(0, 0.35f);
        yield return new WaitForSeconds(time);
        GameObject coin = Instantiate(coinPrefab, moneyDes);
        coin.transform.localScale = new Vector3(1.4f, 1.4f, 1.4f);
        coin.transform.position = moneySpawn.position + new Vector3(Random.Range(-20, 20), Random.Range(-20, 20), 0);
        yield return new WaitForSeconds(0.35f - time + i * 0.04f);

        coinMove.Insert(0, coin.transform.DOMove(moneyDes.position, 0.4f).OnComplete(() =>
        {
            Destroy(coin.gameObject);
        }));
    }

    public void OnButtonClaim30Video()
    {
        //ads
        if (AdsManager.instance.isRewardAvailable())
        {
            AdsManager.instance.TypeReward = "Claim30";
            AdsManager.instance.ShowAdsRewards();
        }
        else
        {
            Debug.Log("No ads available");
            SceneManager.LoadScene("PopUpAds", LoadSceneMode.Additive);
        }
        ConfettiEffect.SetActive(false);
    }

    public void Claim30()
    {
        isClaim30 = true;
    }

    void Claim30Active()
    {
        if (isClaim30)
        {
            isClaim30 = false;
            popupCongrat.SetActive(false);
            UserData.instance.gold += 30;
            UserData.instance.Save();
            GameController.instance.Spawn(nextValue);
            GameController.instance.audioManager.SoundCoin();
            for (int i = 0; i < 10; i++)
            {
                StartCoroutine(CoinAnimate(i, coin10start, coinDes));
            }
        }
    }

    public void ActiveConfetti()
    {
        ConfettiEffect.SetActive(true);
    }

    public void OnButtonRank()
    {
        if (!UiController.instance.isRateStar)
        {
            if (!GameController.instance.activePiggy.isActive)
            {
                GameController.instance.Pause();
                PlayerPrefs.SetString("PrevScene", "Game");
                SceneManager.LoadScene("Rank", LoadSceneMode.Additive);
            }
        }
        else
        {
            UiController.instance.ActiveRateStar();
        }
    }
}
