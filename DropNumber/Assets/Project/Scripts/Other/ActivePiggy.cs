﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class ActivePiggy : MonoBehaviour
{
    public GameObject hammer1;
    public GameObject hammer2;
    public GameObject stock1;
    public GameObject stock2;
    public GameObject free1;
    public GameObject free2;
    public GameObject block1;
    public GameObject imageHammer1;
    public GameObject piggy;
    public GameObject piggyGroup1;
    public GameObject piggyGroup2;

    public GameObject coinPrefab;
    public Transform coinStart;
    public Transform coinDes;
    public Transform startMoveToPig;
    public float timePiggy { get; set; } = 30;
    public TextMeshProUGUI timeText;

    public GameObject PiggyPopupAds;

    public bool isActive = false;
    bool isPause = false;

    // Start is called before the first frame 
    void Start()
    {
        
    }

    private void Update()
    {
        if(isActive)
        {
            if (timePiggy - Time.deltaTime > 0)
            {
                if (!isPause) {
                    timePiggy -= Time.deltaTime;
                    //time = Mathf.Floor((time * 10) / 10);
                    timeText.text = timePiggy.ToString("0.0");
                } 
            }
            else
            {
                DeActive(false);
            }           
        }
    }

    public void Active()
    {
        bool isPopup = PlayerPrefs.GetInt("isPiggyPopup", 1) == 1;
        if(isPopup)
        {
            PlayerPrefs.SetInt("isPiggyPopup", 0);
            isPause = true;
            PiggyPopupAds.SetActive(true);
            GameController.instance.Pause();
        }
        isActive = true;
        float time = 0.3f;
        hammer1.GetComponent<RectTransform>().DOSizeDelta(new Vector2(85, 77.1f) , time);
        hammer2.GetComponent<RectTransform>().DOSizeDelta(new Vector2(85, 77.1f), time);
        hammer1.transform.DOLocalMoveX(67, time);
        hammer2.transform.DOLocalMoveX(122, time);
        stock1.transform.DOLocalMoveX(21, time);
        stock2.transform.DOLocalMoveX(21, time);
        free1.transform.DOLocalMoveX(15, time);
        free1.transform.DOScale(new Vector3(0.8f,0.8f,1), time);
        free2.transform.DOLocalMoveX(15, time);
        free2.transform.DOScale(new Vector3(0.8f, 0.8f, 1), time);
        block1.transform.DOLocalMove(new Vector2(-12.5f, 8.5f), time);
        block1.transform.DOScale(new Vector3(0.8f,0.8f,1), time);
        imageHammer1.transform.DOLocalMove(new Vector2(6.4f, -2.6f), time);
        imageHammer1.transform.DOScale(new Vector3(0.23f, 0.23f, 1), time);

        piggy.GetComponent<RectTransform>().DOSizeDelta(new Vector2(140, 51.8f), time);
        piggy.transform.DOLocalMoveX(31, time);
        piggyGroup1.transform.DOLocalMoveX(-31, time).OnComplete(()=> {
            piggyGroup2.SetActive(true);
        });
    }
    public void DeActive(bool isWatchAds)
    {
        timePiggy = 30;
        float time = 0.1f;
        isActive = false;
        hammer1.GetComponent<RectTransform>().DOSizeDelta(new Vector2(119, 77.1f), time);
        hammer2.GetComponent<RectTransform>().DOSizeDelta(new Vector2(119, 77.1f), time);
        hammer1.transform.DOLocalMoveX(17, time);
        hammer2.transform.DOLocalMoveX(106, time);
        stock1.transform.DOLocalMoveX(36, time);
        stock2.transform.DOLocalMoveX(36, time);
        free1.transform.DOLocalMoveX(28, time);
        free1.transform.DOScale(new Vector3(1, 1, 1), time);
        free2.transform.DOLocalMoveX(28, time);
        free2.transform.DOScale(new Vector3(1, 1, 1), time);
        block1.transform.DOLocalMove(new Vector2(-21f, 0), time);
        block1.transform.DOScale(new Vector3(1, 1, 1), time);
        imageHammer1.transform.DOLocalMove(new Vector2(0.7f, 2), time);
        imageHammer1.transform.DOScale(new Vector3(0.33f, 0.33f, 1), time);

        piggy.GetComponent<RectTransform>().DOSizeDelta(new Vector2(70, 51.8f), time);
        piggy.transform.DOLocalMoveX(-0.6f, time);
        piggyGroup1.transform.DOLocalMoveX(-0, time).OnComplete(() => {
            piggyGroup2.SetActive(false);
            if (isWatchAds)
            {
                StartCoroutine(coinMove());
            }
            else
            {
                StartCoroutine(CoinMoveToPig());
            }
        });
    }
    IEnumerator coinMove()
    {
        for (int i = 0; i < 12; i++)
        {
            StartCoroutine(CoinAnimate(i, coinStart, coinDes));
        }
        yield return new WaitForSeconds(0.5f);
        GameController.instance.audioManager.SoundCoin();
        DOTween.To(xx => UserData.instance.gold = Mathf.FloorToInt(xx), UserData.instance.gold, UserData.instance.gold + 150, 0.4f).OnComplete(() => {
            UserData.instance.Save();
        });
    }

    IEnumerator CoinAnimate(int i, Transform moneySpawn, Transform moneyDes)
    {
        Sequence coinMove = DOTween.Sequence();
        float time = Random.Range(0, 0.5f);
        yield return new WaitForSeconds(time);
        GameObject coin = Instantiate(coinPrefab, moneyDes);
        coin.transform.localScale = new Vector3(1f, 1f, 1f);
        coin.transform.position = moneySpawn.position + new Vector3(Random.Range(-20f, 20f), Random.Range(-20f, 20f), 0);
        yield return new WaitForSeconds(0.35f - time + i * 0.04f);

        coinMove.Insert(0, coin.transform.DOMove(moneyDes.position, 0.4f).OnComplete(() => {
            Destroy(coin.gameObject);
        }));
    }

    IEnumerator CoinMoveToPig()
    {

        CoinAnimateMoveToPig(startMoveToPig);

        yield return new WaitForSeconds(0.8f);
        GameController.instance.audioManager.SoundCoin();
        if (UserData.instance.numPiggy <= 1950)
        {
            DOTween.To(xx => UserData.instance.numPiggy = Mathf.FloorToInt(xx), UserData.instance.numPiggy, UserData.instance.numPiggy + 50, 0.4f).OnComplete(()=> {
                UserData.instance.Save();
            });
        }
        else
        {
            DOTween.To(xx => UserData.instance.numPiggy = Mathf.FloorToInt(xx), UserData.instance.numPiggy, 2000, 0.4f).OnComplete(() =>
            {
                UserData.instance.Save();
            });
        }
    }
    void CoinAnimateMoveToPig(Transform moneySpawn)
    {
        Sequence coinMove = DOTween.Sequence();
        for (int i = 0; i < 6;i++)
        {
            GameObject coin = Instantiate(coinPrefab, moneySpawn);
            coin.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            coin.transform.position = moneySpawn.position;
            coinMove.Insert(0, coin.transform.DOMove(moneySpawn.position + new Vector3(130 * Mathf.Sin((i - 3) * Mathf.PI/13), 120 *Mathf.Cos((i-3)*Mathf.PI / 13), 0), 0.1f));          
            coinMove.Insert(0.6f, coin.transform.DOMove(moneySpawn.position, 0.3f).SetEase(Ease.Linear).OnComplete(() => {
                Destroy(coin.gameObject);
            }));
        }
    }
    public void DeActivePopup()
    {
        isPause = false;
        GameController.instance.Pause();
        PiggyPopupAds.SetActive(false);
        GameController.instance.audioManager.SoundClick();
    }

}
