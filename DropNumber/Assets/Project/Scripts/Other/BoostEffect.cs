﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class BoostEffect : MonoBehaviour
{
    public Image StartEffect1;
    public Transform EffectTransform;
    public GameObject effect2;
    Sequence effect;
    void Start()
    {
        StartCoroutine(StartEffect());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator StartEffect()
    {
        yield return new WaitForSeconds(0.5f);
        StartButtonEffect();
    }
    void StartButtonEffect()
    {
        effect = DOTween.Sequence();
        effect.Insert(0, StartEffect1.DOFade(0.3f, 0.2f));
        effect.Insert(0.2f, StartEffect1.DOFade(0, 2));
        effect.AppendInterval(1);

        int effect2Length = 15;
        float length = 210;
        float heigh = 100;
        GameObject[] eff2 = new GameObject[effect2Length];
        for (int i = 0; i < effect2Length; i++)
        {
            eff2[i] = Instantiate(effect2, EffectTransform);
            eff2[i].transform.localPosition = new Vector3(-length / 2 + i * length / effect2Length, -heigh / 2, 0);
            float x = Random.Range(0.3f, 0.8f);
            eff2[i].transform.localScale = new Vector3(x, x, x);
            effect.Insert(0f, eff2[i].transform.DOLocalMoveY(eff2[i].transform.localPosition.y + Random.Range(100f, 200f), 1f));
            effect.Insert(0.5f, eff2[i].GetComponent<Image>().DOFade(0, 0.5f));
        }
        effect.OnComplete(() =>
        {
            for (int i = 0; i < effect2Length; i++)
            {
                Destroy(eff2[i]);
            }
            effect.Kill();
            StartButtonEffect();
        });
    }

    public void DestroyEffect()
    {
        effect.Kill();
    }
}
