﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BuyButton : MonoBehaviour
{
    public enum ItemType
    {
        Gold600,
        Gold1600,
        Gold3400,
        Gold7200,
        Gold12000,
        Gold48000,
        PiggyBank,
        NoAds,
    }

    public ItemType itemType;
    public TextMeshProUGUI priceText;

    private string defaultText;

    void Start()
    {
        if (priceText != null) {
            defaultText = priceText.text; 
        } 
        StartCoroutine(LoadPriceRoutine());
    }
    
    public void ClickBuy()
    {
        switch(itemType)
        {
            case ItemType.Gold600:Purchaser.instance.Buy600Gold();break;
            case ItemType.Gold1600: Purchaser.instance.Buy1600Gold(); break;
            case ItemType.Gold3400: Purchaser.instance.Buy3400Gold(); break;
            case ItemType.Gold7200: Purchaser.instance.Buy7200Gold(); break;
            case ItemType.Gold12000: Purchaser.instance.Buy12000Gold(); break;
            case ItemType.Gold48000: Purchaser.instance.Buy48000Gold(); break;
            case ItemType.PiggyBank: Purchaser.instance.BuyPiggyBank(); break;
            case ItemType.NoAds: Purchaser.instance.BuyNoAds(); break;
        }
    }

    private IEnumerator LoadPriceRoutine()
    {
        while(Purchaser.instance.IsInitialized())
        {
            yield return null;
        }
        string loadedPrice = "";
        switch (itemType)
        {
            case ItemType.Gold600: loadedPrice = Purchaser.instance.GetProductPriceFromStore(Purchaser.instance.GOLD_600); break;
            case ItemType.Gold1600: loadedPrice = Purchaser.instance.GetProductPriceFromStore(Purchaser.instance.GOLD_1600); break;
            case ItemType.Gold3400: loadedPrice = Purchaser.instance.GetProductPriceFromStore(Purchaser.instance.GOLD_3400); break;
            case ItemType.Gold7200: loadedPrice = Purchaser.instance.GetProductPriceFromStore(Purchaser.instance.GOLD_7200); break;
            case ItemType.Gold12000: loadedPrice = Purchaser.instance.GetProductPriceFromStore(Purchaser.instance.GOLD_12000); break;
            case ItemType.Gold48000: loadedPrice = Purchaser.instance.GetProductPriceFromStore(Purchaser.instance.GOLD_48000); break;
            case ItemType.PiggyBank: loadedPrice = Purchaser.instance.GetProductPriceFromStore(Purchaser.instance.PIGGY_BANK); break;
            case ItemType.NoAds: loadedPrice = Purchaser.instance.GetProductPriceFromStore(Purchaser.instance.NO_ADS); break;
        }
        if (priceText != null)
        {
            priceText.text = defaultText + " " + loadedPrice;
        }
    }
}
