﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Capture : MonoBehaviour
{
    public bool isShowCapture = false;
    public IEnumerator CoCaptureIt()
    {
        yield return new WaitForEndOfFrame();
        Debug.Log("Captured");
        isShowCapture = true;
        string fileName = "DropTheNumberResult.png";
        ScreenCapture.CaptureScreenshot(fileName);
    }

    public void CaptureIt()
    {
        StartCoroutine(CoCaptureIt());
    }
}
