﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class DisableAdsFreeButton : MonoBehaviour
{
    public GameObject Ads;
    public void DeActiveAdsButton()
    {
        Ads.GetComponent<Image>().DOFade(0, 0.28f);
        Ads.transform.DOLocalMoveY(-561, 0.28f);
    }
    public void ActiveAdsButton()
    {
        if(!UserData.instance.isNoAds) {


            Ads.SetActive(true);
            Ads.GetComponent<Image>().DOFade(1, 0.28f);
            Ads.transform.DOLocalMoveY(-420,0.28f).SetEase(Ease.Linear).OnComplete(()=> {
                Ads.transform.DOLocalMoveY(-440, 0.1f).SetEase(Ease.Linear);
            });
        }
        else
        {
            Ads.SetActive(false);
        }
    }
}
