﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixSizeScreen : MonoBehaviour
{
    public GameObject allSprite;
    public GameObject allUi;
    void Awake()
    {
        float x = Screen.width;
        float y = Screen.height;
        if(x/y < 0.5)
        {
            if (allSprite != null)
            {
                allSprite.transform.localScale = new Vector3(0.92f, 0.92f, 0.92f);
            }
            allUi.transform.localScale = new Vector3(0.92f, 0.92f, 0.92f);
        }
    }
}
