﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameEffect : MonoBehaviour
{
    public static GameEffect instance;
    public Camera mainCamera;
    public GameObject effectNumber;

    private void Awake()
    {
        instance = this;
    }
    public void ShowEffectScore(Vector3 position, float value)
    {
        GameObject effect = Instantiate(effectNumber, transform);
        effect.GetComponent<TextMeshProUGUI>().text = "+" + Mathf.Pow(2, value + 1).ToString();
        effect.transform.position = mainCamera.WorldToScreenPoint(position + new Vector3(0,1.88f/2,0));

        effect.GetComponent<TextMeshProUGUI>().DOFade(0, 1.5f);
        effect.transform.DOMoveY(effect.transform.position.y + 20, 1.5f).OnComplete(() =>
        {
            Destroy(effect);
        });
    }
}
