﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuEffect : MonoBehaviour
{
    public Image StartEffect1;
    public Image StartEffect2;
    public Transform EffectTransform;
    public GameObject effect2;

    public Image rankEffect;
    Sequence effect;
    void Start()
    {
        StartCoroutine(StartEffect());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator StartEffect()
    {
        yield return new WaitForSeconds(2);
        StartButtonEffect();
    }
    void StartButtonEffect()
    {
        effect = DOTween.Sequence();
        effect.Insert(0, StartEffect1.DOFade(1, 0.2f));
        effect.Insert(0, StartEffect2.DOFade(1, 0.2f));
        effect.Insert(0.2f, StartEffect1.DOFade(0, 2));
        effect.Insert(0.2f, StartEffect2.DOFade(0, 2));
        effect.Insert(0, rankEffect.DOFade(1, 0.2f));
        effect.Insert(0.2f, rankEffect.DOFade(0, 2));
        effect.AppendInterval(1); 

        int effect2Length = 20;
        float length = 478;
        float heigh = 246;
        GameObject[] eff2 = new GameObject[effect2Length];
        for (int i = 0; i < effect2Length; i++)
        {
            eff2[i] = Instantiate(effect2, EffectTransform);
            eff2[i].transform.localPosition = new Vector3(-length/2 + i * length / effect2Length, -heigh/2, 0);
            float x = Random.Range(0.3f, 0.8f);
            eff2[i].transform.localScale = new Vector3(x, x, x);
            effect.Insert(0f, eff2[i].transform.DOLocalMoveY(eff2[i].transform.localPosition.y + Random.Range(1f, 400f), 1f));
            effect.Insert(0.5f, eff2[i].GetComponent<Image>().DOFade(0, 0.5f));
        }
        effect.OnComplete(() =>
        {
            for (int i = 0; i < effect2Length; i++)
            {
                Destroy(eff2[i]);
            }
            StartButtonEffect();
        });
    }

    public void DesTroyEffect()
    {
        effect.Kill();
    }
}
