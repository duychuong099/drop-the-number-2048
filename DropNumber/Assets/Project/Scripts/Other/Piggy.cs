﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Piggy : MonoBehaviour
{
    public TextMeshProUGUI PiggyText;
    public Slider slider;
    public GameObject gold;
    public Transform des;
    public Transform start;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PiggyText.text = UserData.instance.numPiggy.ToString() + "/2000";
        if (UserData.instance.numPiggy != 0 && UserData.instance.numPiggy < 135)
        {
            slider.value = 135;
        }
        else slider.value = UserData.instance.numPiggy;
    }

    public void Effect()
    {

    }
}
