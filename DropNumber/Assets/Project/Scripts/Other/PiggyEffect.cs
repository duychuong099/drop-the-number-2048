﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.PlayerLoop;

public class PiggyEffect : MonoBehaviour
{
    public GameObject effect;
    public float range;
    public float scalemin;
    public float scalemax;
    public float timeDelta = 0.45f;

    public float time = 0;

    // Update is called once per frame
    private void Start()
    {

    }

    private void Update()
    {
        if (time > timeDelta)
        {
            time = 0;
            GameObject eff = Instantiate(effect, transform);
            eff.transform.position = transform.position + new Vector3(Random.Range(-range, range), Random.Range(-range, range), 0);
            eff.transform.localScale = new Vector3(Random.Range(scalemin, scalemax), Random.Range(scalemin, scalemax), 1);
            eff.GetComponent<Image>().DOFade(1, 1f).OnComplete(() => {
                if (eff != null)
                {
                    eff.GetComponent<Image>().DOFade(0, 1f).OnComplete(() =>
                    {
                        if (eff != null)
                        {
                            Destroy(eff);
                        }
                    });
                }
            });
        }
        else time += Time.deltaTime;
    }

    IEnumerator EffectPig()
    {
        while (true)
        {
            GameObject eff = Instantiate(effect, transform);
            eff.transform.position = transform.position + new Vector3(Random.Range(-range, range), Random.Range(-range, range), 0);
            eff.transform.localScale = new Vector3(Random.Range(scalemin, scalemax), Random.Range(scalemin, scalemax), 1);
            eff.GetComponent<Image>().DOFade(1, 1f).OnComplete(() => {
                if (eff != null) {
                    eff.GetComponent<Image>().DOFade(0, 1f).OnComplete(() =>
                    {
                        if (eff != null)
                        {
                            Destroy(eff);
                        }
                    }); 
                } 
            });
            yield return new WaitForSeconds(0.4f);
        }
    }
}
