﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TapToPlayAnim : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Scale();
    }

    void Scale()
    {
        gameObject.transform.DOScale(1.1f, 0.4f).OnComplete(() => {
            gameObject.transform.DOScale(1f, 0.1f).OnComplete(() => { Scale(); });
        });
    }
}
