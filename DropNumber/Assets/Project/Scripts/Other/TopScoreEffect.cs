﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class TopScoreEffect : MonoBehaviour
{
    public Image topScoreEffect;
    Sequence effect;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartEffect());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator StartEffect()
    {
        yield return new WaitForSeconds(1);
        StartButtonEffect();
    }

    void StartButtonEffect()
    {
        effect = DOTween.Sequence();
        effect.Insert(0, topScoreEffect.DOFade(1, 0.2f));
        effect.Insert(0.5f, topScoreEffect.DOFade(0, 1));

        effect.AppendInterval(0.75f);

        effect.OnComplete(() =>
        {
            StartButtonEffect();
        });
    }

    public void DestroyEffect()
    {
        effect.Kill();
    }
}
