﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReferenceManager : MonoBehaviour
{
    public static ReferenceManager instance;

    private void Awake()
    {
        instance = this;
    }

    public Sprite[] numberSprite;
    public Color[] effectSpeedUp;
}
