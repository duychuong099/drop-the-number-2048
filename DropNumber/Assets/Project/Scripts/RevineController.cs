﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class RevineController : MonoBehaviour
{
    public static RevineController instance;

    private void Awake()
    {
        instance = this;
    }

    public TextMeshProUGUI goldText;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI highscoreText;

    // Start is called before the first frame update
    void Start()
    {
        goldText.text = UserData.instance.gold.ToString();
        highscoreText.text = UserData.instance.topScore.ToString();
        scoreText.text = GameController.instance.currentScore.ToString();
    }

    private void Update()
    {
        goldText.text = UserData.instance.gold.ToString();
    }

    public void OnButtonReviveVideo()  //ads
    {
        if (AdsManager.instance.isRewardAvailable())
        {
            AdsManager.instance.TypeReward = "Revine";
            AdsManager.instance.ShowAdsRewards();
            
        }
        else
        {
            Debug.Log("No ads available");
            SceneManager.LoadScene("PopUpAds", LoadSceneMode.Additive);
        }
    }

    public void Revine() {
        GameController.instance.ReviveAction();
        gameObject.SetActive(false);
    }

    public void OnButtonReviveGold()
    {
        if (UserData.instance.gold < 100)
        {
            OnButtonShop();
        }
        else
        {
            UserData.instance.gold -= 100;
            UserData.instance.Save();
            GameController.instance.ReviveAction();
            gameObject.SetActive(false);
        }
    }
    public void OnButtonBack()
    {
        if(GameController.instance.isTopScore)
        {
            GameController.instance.ActivetopScore();
            gameObject.SetActive(false);
        }
        else
        {
            GameController.instance.FinishPlayed();
            UserData.instance.isResultCoinMove = true;
            SceneManager.LoadScene("GameResult");
        }
    }

    public void OnButtonShop()
    {
        PlayerPrefs.SetString("PrevScene", "Revine");
        SceneManager.LoadScene("Shop", LoadSceneMode.Additive);
    }
}
