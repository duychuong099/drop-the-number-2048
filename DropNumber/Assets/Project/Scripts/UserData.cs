﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class UserData : MonoBehaviour
{
    public static UserData instance;

    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this);
        isSound = PlayerPrefs.GetInt("isSound", 1) == 1;
        isVibrate = PlayerPrefs.GetInt("isVibrate", 1) == 1;
    }

    public int gold;
    public int topScore;
    public int rank;
    public int gamePlayed;
    public int[] blockNum;
    public int numColorHammer;
    public int numHammer;
    public int numPiggy;
    public bool isSound;
    public bool isVibrate;
    public bool isFirstStartMenu;
    public bool isNoAds;
    public bool isShowCapture = false;
    public bool isResultCoinMove = false;
    public bool isNextPause = true;
    public bool isPlaying;
    public void Save()
    {
        SaveObject saveObject = new SaveObject
        {
            goldAmount = gold,
            topScore = topScore,
            rank = rank,
            gamePlayed = gamePlayed,
            blockNum = blockNum,
            numColorHammer = numColorHammer,
            numHammer = numHammer,
            numPiggy = numPiggy,
            isNoAds = isNoAds,
            isPlaying = isPlaying,
        };  
        string json = JsonUtility.ToJson(saveObject);
        System.IO.File.WriteAllText(Application.persistentDataPath + "/save.txt", json);
    }
    public void Load()
    {
        if(File.Exists(Application.persistentDataPath + "/save.txt"))
        {
            //TextAsset textAsset = Resources.Load(Application.persistentDataPath + "/save.txt") as TextAsset;
            string saveString = File.ReadAllText(Application.persistentDataPath + "/save.txt");
            SaveObject saveObject = JsonUtility.FromJson<SaveObject>(saveString);

            gold = saveObject.goldAmount;
            topScore = saveObject.topScore;
            //rank = saveObject.rank;
            rank = saveObject.rank;
            gamePlayed = saveObject.gamePlayed;            
            blockNum = saveObject.blockNum;
            numColorHammer = saveObject.numColorHammer;
            numHammer = saveObject.numHammer;
            numPiggy = saveObject.numPiggy;
            isNoAds = saveObject.isNoAds;
            isPlaying = saveObject.isPlaying;
        }
    }

    public void AddGold(int Amount)
    {
        gold += Amount;
        Save();
    }
    public void RemoveAds()
    {
        isNoAds = true;
        AdsManager.instance.DestroyBanner();
        GameController.instance.adsfree.SetActive(false);
        Save();
    }

    public void PiggyBank()
    {
        numPiggy = 0;
        AddGold(2000);
    }
}

public class SaveObject
{
    public int goldAmount;
    public int topScore;
    public int rank;
    public int gamePlayed;
    public int[] blockNum;
    public int numColorHammer;
    public int numHammer;
    public int numPiggy;
    public bool isNoAds;

    //pause back
    public bool isPlaying;
}
